// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  base_url: '/atahualpa_server/API/',
  error_required: ' es requerido',
  error_email_invalid: ' no es un email válido',
  role_commerce: 2,
  role_admin: 1,
  template_type_storage: 1,
  template_type_extra_data: 2
};
