export const environment = {
  production: true,
  base_url: '/atahualpa_server/API/',
  error_required: ' es requerido',
  error_email_invalid: ' no es un email válido',
  role_commerce: 2,
  role_admin: 1,
  template_type_storage: 1,
  template_type_extra_data: 2

};
