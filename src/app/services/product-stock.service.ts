import {Supplier} from '../modules/supplier';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from '../modules/message-type';
import { ProductStock } from '../modules/product-stock';
import { ProductSearch } from '../modules/product-search';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProductStockService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router
  ) {
      super(http, messageService, router);
   }

   private productStockUrl = this.base_url + 'stock/';

   //////// Get methods //////////

  /** GET login to the server */
  getProductStocks(productId: number): Observable<ProductStock[]> {
    const url = this.productStockUrl + 'product/' + productId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los Stocks de los Productos.';

    return this.http.get<ProductStock[]>(url, options).pipe(
      catchError(this.handleError('getProductStocks', errorMessage, []))
    );
  }

  searchProducts(productSearch: string): Observable<ProductSearch[]> {

    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
	const params = {
		productSearch: productSearch
	};
	// TODO: ver si se puede mandar de otra manera
	const url = this.productStockUrl + 'product/search?productSearch=' + params.productSearch;
    const errorMessage = 'Hubo un error al intentar recuperar los Stocks de los Productos.';

    return this.http.get<ProductSearch[]>(url, options).pipe(
      catchError(this.handleError('searchProducts', errorMessage, []))
    );
  }

  saveProductStocks(productStocks: ProductStock[]): Observable<Response> {
    const url = this.productStockUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'Los Stocks del Producto fueron guardados correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar los Stocks del Producto.';

    return this.http.put<Response>(url, productStocks, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveProductStocks', errorMessage, null))
      );

  }

}
