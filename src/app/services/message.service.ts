import { Message } from '../modules/message';
import { Injectable } from '@angular/core';
import { Observable ,  BehaviorSubject } from "rxjs";
import { MessageType } from "../modules/message-type";

@Injectable()
export class MessageService { 

  public message: BehaviorSubject<Message>;
  
  constructor() {
	    this.message = new BehaviorSubject<Message>(new Message("",MessageType.Info));
	}

  add(message: Message) {
	  this.message.next(message);
  }
  
  clear() {
    this.message = null;
  }
  
  getMessage(): Observable<Message> {
	  return this.message.asObservable();
  }
    
}
