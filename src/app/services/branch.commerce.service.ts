import {BranchCommerce} from '../modules/branch-commerce';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { MessageType } from '../modules/message-type';
import { catchError, tap } from 'rxjs/operators';


@Injectable()
export class BranchCommerceService extends ParentService {
  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
    super(http, messageService, router);
  }

  private branchCommerceUrl = this.base_url + 'branch-commerce/';

  //////// Get methods //////////

  /** GET login to the server */
  getBranchCommerces(commerceId: number): Observable<BranchCommerce[]> {
    let url = this.branchCommerceUrl + 'all';
    if (commerceId) {
      url += '/commerce/' + commerceId;
    }
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar las Sucursales.';

    return this.http.get<BranchCommerce[]>(url, options).pipe(
      catchError(this.handleError('getBranchCommerces', errorMessage, []))
    );
  }

  getBranchCommerce(id: string): Observable<BranchCommerce> {
    const url = this.branchCommerceUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar la Sucursal.';

    return this.http.get<BranchCommerce>(url, options).pipe(
      catchError(this.handleError('getBranchCommerce', errorMessage, new BranchCommerce()))
    );
  }

  saveBranchCommerce(branchCommerce: BranchCommerce, commerceId: number): Observable<Response> {
    let url = this.branchCommerceUrl + 'save';

    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'La Sucursal fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar la Sucursal.';
    if (branchCommerce.id) {
      return this.http.put<Response>(url, branchCommerce, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveBranchCommerce', errorMessage, new Response()))
      );
    }
    if (commerceId) {
      url += '/commerce/' + commerceId;
    }

    return this.http.post<Response>(url, branchCommerce, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveBranchCommerce', errorMessage, new Response()))
    );
  }

  deleteBranchCommerce(branchCommerceId: number): Observable<Response> {
    const url = this.branchCommerceUrl + 'remove/' + branchCommerceId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'La Sucursal fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar la Sucursal.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deletBranchCommerce', errorMessage, new Response()))
    );
  }

}

