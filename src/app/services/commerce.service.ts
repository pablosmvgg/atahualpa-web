import {Commerce} from '../modules/commerce';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from "../modules/message-type";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class CommerceService extends ParentService {
  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
    super(http, messageService, router);
  }

  private commerceUrl = this.base_url + 'commerce/';

  ////////Get methods //////////
  getCommerces(): Observable<Commerce[]> {
    const url = this.commerceUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los comercios.';

    return this.http.get<Commerce[]>(url, options).pipe(
      catchError(this.handleError('getCommerces', errorMessage, []))
    );
  }
  
  getCommerce(): Observable<Commerce> {
    const url = this.commerceUrl;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el Comercio.';

    return this.http.get<Commerce>(url, options).pipe(
      catchError(this.handleError('getCommerce', errorMessage, new Commerce()))
    );
  }

    getCommerceById(commerceId: number): Observable<Commerce> {
    const url = this.commerceUrl + commerceId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el Comercio.';

    return this.http.get<Commerce>(url, options).pipe(
      catchError(this.handleError('getCommerce', errorMessage, new Commerce()))
    );
  }

  saveCommerce(commerce: Commerce): Observable<Response> {
    const url = this.commerceUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Comercio fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Proveedor.';
    
    
    if (commerce.id) {
      return this.http.put<Response>(url, commerce, options).pipe(
        tap(branchCommerce => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveCommerce', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, commerce, options).pipe(
      tap(commerce => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveCommerce', errorMessage, null))
    );
    
  }
  
  ////////Delete methods //////////
  deleteCommerce(commerceId: number): Observable<Response> {
    const url = this.commerceUrl + 'remove/' + commerceId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Comercio fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Comercio.';
    return this.http.delete<Response>(url, options).pipe(
      tap(product => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteCommerce', errorMessage, null))
    );
  }

}

