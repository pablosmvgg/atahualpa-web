import {Injectable} from '@angular/core';
import {User} from '../modules/user';
import {UserSession} from '../modules/user-session';
import {MessageService} from './message.service';
import {Observable} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import { MessageType } from '../modules/message-type';

@Injectable()
export class UserService extends ParentService {
  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
    super(http, messageService, router);
  }

  private userUrl = this.base_url + 'user/';  // URL to web api

  //////// Get methods //////////

  /** GET login to the server */
  loginUser(username: string, password: string): Observable<UserSession> {
    const url = this.userUrl + 'login';
    const options = {
      headers: {
        Authorization: 'Basic ' + btoa(username + ':' + password),
        observe: 'response'
      },
      withCredentials: true
    };
    const errorMessage = 'Usuario y/o contraseña incorrectos.';

    return this.http.get<UserSession>(url, options).pipe(
      catchError(this.handleError<UserSession>('loginUser id=${username}', errorMessage))
    );
  }

  getLoggedUser(): Observable<User> {
    const url = this.userUrl + 'loggedUser';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const errorMessage = 'Hubo un error al recuperar el Usuario. Si el problema persiste consulte al administrador.';

    return this.http.get<User>(url, options).pipe(
      catchError(this.handleError<User>('loginUser', errorMessage))
    );
  }

  getUsers(commerceId: number): Observable<User[]> {
    let url = this.userUrl + 'commerce';
    if (commerceId) {
      url += '/' + commerceId;
    }
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const errorMessage = 'Hubo un error al recuperar los Usuarios. Si el problema persiste consulte al administrador.';

    return this.http.get<User[]>(url, options).pipe(
      catchError(this.handleError<User[]>('getUsers', errorMessage))
    );
  }

  logout(): Observable<void> {
    const url = this.userUrl + 'logout';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const errorMessage = 'Hubo un error al cerrar la sesión. Si el problema persiste consulte al administrador.';

    return this.http.get<void>(url, options).pipe(
      catchError(this.handleError<void>('error logout', errorMessage))
    );
  }

  getUserByCommerce(id: number): Observable<User> {
    const url = this.userUrl + id + '/commerce';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el Usuario.';

    return this.http.get<User>(url, options).pipe(
      catchError(this.handleError('getBranchCommerce', errorMessage, new User()))
    );
  }

  saveUser(user: User, commerceId: number): Observable<Response> {
    let url = this.userUrl + 'commerce/save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Usuario fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Usuario.';
    if (user.id) {
      return this.http.put<Response>(url, user, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveUser', errorMessage, null))
      );
    }
    if (commerceId) {
      url = this.userUrl + 'commerce/' + commerceId + '/save';
    }

    return this.http.post<Response>(url, user, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveBranchCommerce', errorMessage, null))
    );
  }

  deleteUser(userId: number): Observable<Response> {
    const url = this.userUrl + 'commerce/remove/' + userId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Usuario fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Usuario.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deletUser', errorMessage, null))
    );
  }

}
