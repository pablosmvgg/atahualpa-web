import { Injectable } from '@angular/core';
import { ParentService } from './parent.service';
import { HttpClient } from '@angular/common/http';
import { MessageService } from './message.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductStockType } from '../modules/product-stock-type';
import { catchError, tap } from 'rxjs/operators';
import { MessageType } from '../modules/message-type';

@Injectable({
  providedIn: 'root'
})
export class ProductStockTypeService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router
  ) {
      super(http, messageService, router);
  }

  private productStockTypeUrl = this.base_url + 'product-stock-type/';

  //////// Get methods //////////
  getProductStockTypes(): Observable<ProductStockType[]> {
    const url = this.productStockTypeUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los tipo de stocks.';

    return this.http.get<ProductStockType[]>(url, options).pipe(
      catchError(this.handleError('getProductStockTypes', errorMessage, []))
    );
  }

  getProductStockType(id: string): Observable<ProductStockType> {
    const url = this.productStockTypeUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el tipo de stock.';

    return this.http.get<ProductStockType>(url, options).pipe(
      catchError(this.handleError('getProductStockType', errorMessage, new ProductStockType()))
    );
  }

  //////// Post methods //////////
  //////// Put methods //////////
  saveProductStockType(productStockType: ProductStockType): Observable<Response> {
    const url = this.productStockTypeUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de Stock fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Tipo de Stock.';
    if (productStockType.id) {
      return this.http.put<Response>(url, productStockType, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveProductStockType', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, productStockType, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveProductStockType', errorMessage, null))
    );
  }

  //////// Delete methods //////////
  deleteProductStockType(productStockTypeId: number): Observable<Response> {
    const url = this.productStockTypeUrl + 'remove/' + productStockTypeId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de Stock fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Tipo de Stock.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteProductStockType', errorMessage, null))
    );
  }

}
