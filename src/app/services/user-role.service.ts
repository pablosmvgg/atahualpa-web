import { Injectable } from '@angular/core';
import {ParentService} from './parent.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessageService} from './message.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {UserRole} from '../modules/user-role';
import { catchError } from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserRoleService extends ParentService {

  constructor(protected http: HttpClient,
		    protected messageService: MessageService,
		    protected router: Router) {
	  super(http, messageService, router);
  }
  
  private userRoleUrl = this.base_url + 'user-role/';  // URL to web api

  private errorMessage:string = 'Hubo un error al recuperar los Roles de los Usuarios. Si el problema persiste consulte al administrador.';
  
  //////// Get methods //////////
  getUserCommerceRoles(): Observable<UserRole[]> {
	const url = this.userRoleUrl+'commerce';
	const options = {
	  headers: {
	    observe: 'response'
	  },
	  withCredentials: true
	};
	
	return this.http.get<UserRole[]>(url, options).pipe(
	  catchError(this.handleError<UserRole[]>('getUserCommerceRoles', this.errorMessage))
    );
  }
  
  getRolesForLoggedUser(): Observable<UserRole[]> {
		const url = this.userRoleUrl+'user';
		const options = {
		  headers: {
		    observe: 'response'
		  },
		  withCredentials: true
		};
		
		return this.http.get<UserRole[]>(url, options).pipe(
		  catchError(this.handleError<UserRole[]>('getRolesForLoggedUser', this.errorMessage))
	    );
	  }

}
