import {Product} from '../modules/product';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import { MessageType } from '../modules/message-type';

@Injectable()
export class ProductService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
      super(http, messageService, router);
  }

  private productUrl = this.base_url + 'product/';

  //////// Get methods //////////
  getProducts(): Observable<Product[]> {
    const url = this.productUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los productos.';

    return this.http.get<Product[]>(url, options).pipe(
      catchError(this.handleError('getProducts', errorMessage, []))
    );
  }

  getProduct(id: string): Observable<Product> {
    const url = this.productUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el producto.';

    return this.http.get<Product>(url, options).pipe(
      catchError(this.handleError('getProduct', errorMessage, new Product()))
    );
  }

  //////// Post methods //////////
  //////// Put methods //////////
  saveProduct(product: Product): Observable<Response> {
    const url = this.productUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Producto fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Producto.';
    if (product.id) {
      return this.http.put<Response>(url, product, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveProduct', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, product, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveProduct', errorMessage, null))
    );
  }

  //////// Delete methods //////////
  deleteProduct(productId: number): Observable<Response> {
    const url = this.productUrl + 'remove/' + productId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Producto fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Producto.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteProduct', errorMessage, null))
    );
  }

}
