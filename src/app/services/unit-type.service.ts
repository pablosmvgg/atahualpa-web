import {UnitType} from '../modules/unit-type';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import { MessageType } from '../modules/message-type';

@Injectable()
export class UnitTypeService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
      super(http, messageService, router);
  }

  private unitTypeUrl = this.base_url + 'unit-type/';

  //////// Get methods //////////
  getUnitTypes(): Observable<UnitType[]> {
    const url = this.unitTypeUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los tipos de unidad.';

    return this.http.get<UnitType[]>(url, options).pipe(
      catchError(this.handleError('getUnitTypes', errorMessage, []))
    );
  }

  getUnitType(id: string): Observable<UnitType> {
    const url = this.unitTypeUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el tipo de unidad.';

    return this.http.get<UnitType>(url, options).pipe(
      catchError(this.handleError('getUnitType', errorMessage, new UnitType()))
    );
  }

  //////// Post methods //////////
  //////// Put methods //////////
  saveUnitType(unitType: UnitType): Observable<Response> {
    const url = this.unitTypeUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de unidad fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Tipo de Unidad.';
    if (unitType.id) {
      return this.http.put<Response>(url, unitType, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveUnitType', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, unitType, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveUnitType', errorMessage, null))
    );
  }

  //////// Delete methods //////////
  deleteUnitType(unitTypeId: number): Observable<Response> {
    const url = this.unitTypeUrl + 'remove/' + unitTypeId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de Unidad fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Tipo de Unidad.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteUnitType', errorMessage, null))
    );
  }

}
