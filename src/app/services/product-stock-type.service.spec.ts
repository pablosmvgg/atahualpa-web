import { TestBed } from '@angular/core/testing';

import { ProductStockTypeService } from './product-stock-type.service';

describe('ProductStockTypeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductStockTypeService = TestBed.get(ProductStockTypeService);
    expect(service).toBeTruthy();
  });
});
