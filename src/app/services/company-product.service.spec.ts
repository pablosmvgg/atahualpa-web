import { TestBed, inject } from '@angular/core/testing';

import { CompanyProductService } from './company-product.service';

describe('CompanyProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyProductService]
    });
  });

  it('should be created', inject([CompanyProductService], (service: CompanyProductService) => {
    expect(service).toBeTruthy();
  }));
});
