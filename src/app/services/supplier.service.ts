import {Supplier} from '../modules/supplier';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from '../modules/message-type';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class SupplierService extends ParentService {
  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
    super(http, messageService, router);
  }

  private supplierUrl = this.base_url + 'supplier/';  // URL to web api

  //////// Get methods //////////

  /** GET login to the server */
  getSuppliers(): Observable<Supplier[]> {
    const url = this.supplierUrl + 'all';
    const options = {
      headers: {
        // Authorization: 'Basic ' + btoa(username + ':' + password),
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los Proveedores.';

    return this.http.get<Supplier[]>(url, options).pipe(
      catchError(this.handleError('getSuppliers', errorMessage, []))
    );
  }

  getSupplier(id: string): Observable<Supplier> {
    const url = this.supplierUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el Proveedor.';

    return this.http.get<Supplier>(url, options).pipe(
      catchError(this.handleError('getSupplier', errorMessage, new Supplier()))
    );
  }

  saveSupplier(supplier: Supplier): Observable<Response> {
    const url = this.supplierUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Proveedor fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Proveedor.';
    if (supplier.id) {
      return this.http.put<Response>(url, supplier, options).pipe(
        tap(suppliers => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveSupplier', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, supplier, options).pipe(
      tap(suppliers => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveSupplier', errorMessage, null))
    );
  }

  deleteSupplier(supplierId: number): Observable<Response> {
    const url = this.supplierUrl + 'remove/' + supplierId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Proveedor fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Proveedor.';
    return this.http.delete<Response>(url, options).pipe(
      tap(suppliers => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveSupplier', errorMessage, null))
    );
  }

}

