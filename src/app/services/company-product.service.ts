import {CompanyProduct} from '../modules/company-product';
import {UnitType} from '../modules/unit-type';
import {Response} from '../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from "../modules/message-type";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class CompanyProductService extends ParentService {

  constructor(
    protected http: HttpClient,
	protected messageService: MessageService,
	protected router: Router) {
	  super(http, messageService, router);
  }
  
private companyProductUrl = this.base_url + 'company-product/';
  
  ////////Get methods //////////
  getCompanyProducts(): Observable<CompanyProduct[]> {
    const url = this.companyProductUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los productos.';

    return this.http.get<CompanyProduct[]>(url, options).pipe(
      catchError(this.handleError('getCompanyProducts', errorMessage, []))
    );
  }
  
  getCompanyProduct(id: string): Observable<CompanyProduct> {
    const url = this.companyProductUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el producto.';

    return this.http.get<CompanyProduct>(url, options).pipe(
      catchError(this.handleError('getCompanyProduct', errorMessage, new CompanyProduct()))
    );
  }
  
  ////////Post methods //////////
  ////////Put methods //////////
  saveCompanyProduct(companyProduct: CompanyProduct): Observable<Response> {
    const url = this.companyProductUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Producto fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Producto.';
    if (companyProduct.id) {
      return this.http.put<Response>(url, companyProduct, options).pipe(
        tap(companyProduct => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveCompanyProduct', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, companyProduct, options).pipe(
      tap(companyProduct => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveCompanyProduct', errorMessage, null))
    );
  }
  
  
  ////////Delete methods //////////
  deleteCompanyProduct(companyProductId: number): Observable<Response> {
    const url = this.companyProductUrl + 'remove/' + companyProductId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Producto fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Producto.';
    return this.http.delete<Response>(url, options).pipe(
      tap(product => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteCompanyProduct', errorMessage, null))
    );
  }

}
