import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Message} from '../modules/message';
import { environment } from '../../environments/environment';
import { MessageType } from '../modules/message-type';

export class ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {}

  protected base_url = environment.base_url;

  /** Log a HeroService message with the MessageService */
  protected log(messageString: string, messageType: MessageType) {
    const message = new Message(messageString, messageType);
    this.messageService.add(message);
  }

  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  protected handleError<T>(operation = 'operation', errorMessage: string, result?: T) {
    return (error: any): Observable<T> => {

      if (error.status === 401 || error.status === 405) {
        this.log('El usuario es incorrecto o se le expiró la sesión.', MessageType.Error);
        this.forceLogout();
      }
      else if (error.status === 400 && error.error.errorType === 'USER') {
        this.log(error.error.messages[0], MessageType.Error);
        console.error(error.error.messages[0]);
      }
      else {
        console.error(error);
        this.log(errorMessage, MessageType.Error);
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  forceLogout() {
    sessionStorage.removeItem('userToken');
    this.router.navigate(['login']);
  }

}
