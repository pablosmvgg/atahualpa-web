import {TemplateType} from '../../modules/templates/template-type';
import {Response} from '../../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './../message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './../parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from "../../modules/message-type";

@Injectable({
  providedIn: 'root'
})
export class TemplateTypeService extends ParentService {

  constructor(
    protected http: HttpClient,
	protected messageService: MessageService,
	protected router: Router) {
	super(http, messageService, router);
  }
  
  private templateTypeUrl = this.base_url + 'template-type/';
  
  ////////Get methods //////////
  getTemplateTypes(): Observable<TemplateType[]> {
    const url = this.templateTypeUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los tipo de templates.';

    return this.http.get<TemplateType[]>(url, options).pipe(
      catchError(this.handleError('getTemplateTypes', errorMessage, []))
    );
  }
  
  getTemplateType(id: number): Observable<TemplateType> {
    const url = this.templateTypeUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el tipo de template.';

    return this.http.get<TemplateType>(url, options).pipe(
      catchError(this.handleError('getTemplateType', errorMessage, null))
    );
  }
}
