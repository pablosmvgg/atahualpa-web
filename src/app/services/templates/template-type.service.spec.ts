import { TestBed } from '@angular/core/testing';

import { TemplateTypeService } from './template-type.service';

describe('TemplateTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateTypeService = TestBed.get(TemplateTypeService);
    expect(service).toBeTruthy();
  });
});
