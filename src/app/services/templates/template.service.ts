import {Template} from '../../modules/templates/template';
import {Response} from '../../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './../message.service';
import {Observable} from 'rxjs';
import {ParentService} from './../parent.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import { MessageType } from '../../modules/message-type';

@Injectable({
  providedIn: 'root'
})
export class TemplateService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router) {
      super(http, messageService, router);
  }

  private templateUrl = this.base_url + 'template/';

  //////// Get methods //////////
  getTemplateByType(templateTypeId: number): Observable<Template[]> {
    const url = this.templateUrl + 'template-type/' + templateTypeId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los templates.';

    return this.http.get<Template[]>(url, options).pipe(
      catchError(this.handleError('getTemplateByType', errorMessage, null))
    );
  }

  //////// Post methods //////////
  //////// Put methods //////////
  saveTemplate(template: Template): Observable<Response> {
    const url = this.templateUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'Opciones guardadas correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar las Opciones.';
    if (template.id) {
      return this.http.put<Response>(url, template, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveTemplate', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, template, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveTemplate', errorMessage, null))
    );
  }

  saveTemplates(templates: Template[]): Observable<Response> {
    const url = this.templateUrl + 'saves';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'Opciones guardadas correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar las Opciones.';
    const templateFound = templates.find(template => {
        // TODO: ver si se puede evitar esto y usar el truly falsy
        return template.id !== null || template.id !== undefined;
    });
    if (templateFound) {
      return this.http.put<Response>(url, templates, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveTemplate', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, templates, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveTemplate', errorMessage, null))
    );
  }

  //////// Delete methods //////////
  deleteTemplate(templateId: number): Observable<Response> {
    const url = this.templateUrl + 'remove/' + templateId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Template fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Template.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteTemplate', errorMessage, null))
    );
  }

  deleteTemplatesByType(templateTypeId: number): Observable<Response> {
    const url = this.templateUrl + 'remove/templateType/' + templateTypeId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'Los Templates fueron eliminados correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar los Templates.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteTemplatesByType', errorMessage, null))
    );
  }

}
