import {TemplateValueType} from '../../modules/templates/template-value-type';
import {Response} from '../../modules/response';
import {Injectable} from '@angular/core';
import {MessageService} from './../message.service';
import {Observable, of} from 'rxjs';
import {ParentService} from './../parent.service';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import { MessageType } from "../../modules/message-type";

@Injectable({
  providedIn: 'root'
})
export class TemplateValueTypeService extends ParentService {

  constructor(
	protected http: HttpClient,
	protected messageService: MessageService,
	protected router: Router) {
	  super(http, messageService, router);
  }
  
  private templateValueTypeUrl = this.base_url + 'template-value-type/';
  
  ////////Get methods //////////
  getTemplateValueTypes(): Observable<TemplateValueType[]> {
    const url = this.templateValueTypeUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los tipo de valores de templates.';

    return this.http.get<TemplateValueType[]>(url, options).pipe(
      catchError(this.handleError('getTemplateValueTypes', errorMessage, []))
    );
  }
  
  getTemplateValueType(id: string): Observable<TemplateValueType> {
    const url = this.templateValueTypeUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el tipo de valor de template.';

    return this.http.get<TemplateValueType>(url, options).pipe(
      catchError(this.handleError('getTemplateValueType', errorMessage, null))
    );
  }
  
}
