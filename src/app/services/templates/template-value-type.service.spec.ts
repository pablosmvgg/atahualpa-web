import { TestBed } from '@angular/core/testing';

import { TemplateValueTypeService } from './template-value-type.service';

describe('TemplateValueTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateValueTypeService = TestBed.get(TemplateValueTypeService);
    expect(service).toBeTruthy();
  });
});
