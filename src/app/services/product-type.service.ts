import { Injectable } from '@angular/core';
import { ParentService } from './parent.service';
import { Router } from '@angular/router';
import { MessageService } from './message.service';
import { MessageType } from '../modules/message-type';
import { Observable } from 'rxjs';
import { ProductType } from '../modules/product-type';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductTypeService extends ParentService {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService,
    protected router: Router
  ) {
      super(http, messageService, router);
  }

  private productTypeUrl = this.base_url + 'product-type/';

  //////// Get methods //////////
  getProductTypes(): Observable<ProductType[]> {
    const url = this.productTypeUrl + 'all';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar los tipo de Productos.';

    return this.http.get<ProductType[]>(url, options)
    .pipe(
      catchError(this.handleError('getProductTypes', errorMessage, []))
    );
  }

  getProductType(id: string): Observable<ProductType> {
    const url = this.productTypeUrl + id;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };

    const errorMessage = 'Hubo un error al intentar recuperar el tipo de Producto.';

    return this.http.get<ProductType>(url, options).pipe(
      catchError(this.handleError('getProductType', errorMessage, new ProductType()))
    );
  }

  //////// Post methods //////////
  //////// Put methods //////////
  saveProductType(productType: ProductType): Observable<Response> {
    const url = this.productTypeUrl + 'save';
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de Producto fue guardado correctamente.';
    const errorMessage = 'Hubo un error al intentar guardar el Tipo de Producto.';
    if (productType.id) {
      return this.http.put<Response>(url, productType, options).pipe(
        tap(() => this.log(successMessage, MessageType.Success)),
        catchError(this.handleError('saveProductType', errorMessage, null))
      );
    }

    return this.http.post<Response>(url, productType, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('saveProductType', errorMessage, null))
    );
  }

  //////// Delete methods //////////
  deleteProductType(productTypeId: number): Observable<Response> {
    const url = this.productTypeUrl + 'remove/' + productTypeId;
    const options = {
      headers: {
        observe: 'response'
      },
      withCredentials: true
    };
    const successMessage = 'El Tipo de Producto fue eliminado correctamente.';
    const errorMessage = 'Hubo un error al intentar eliminar el Tipo de Producto.';
    return this.http.delete<Response>(url, options).pipe(
      tap(() => this.log(successMessage, MessageType.Success)),
      catchError(this.handleError('deleteProductType', errorMessage, null))
    );
  }
}
