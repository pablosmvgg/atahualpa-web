import { AuthGuard } from './auth.guard';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from './components/dashboard/dashboard.component';

import {LoginPageComponent} from './components/login-page/login-page.component';
import {SupplierComponent} from './components/supplier/supplier.component';
import {SuppliersComponent} from './components/suppliers/suppliers.component';
import {CommercesComponent} from './components/commerces/commerces.component';
import {CommerceComponent} from './components/commerce/commerce.component';
import { BranchesComponent } from './components/branches/branches.component';
import { BranchComponent } from './components/branch/branch.component';
import { LoggedComponent } from './components/logged/logged.component';
import { ProductComponent } from './components/product/product.component';
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import { ProductsComponent } from './components/products/products.component';
import { CompanyProductsComponent } from './components/company-products/company-products.component';
import { CompanyProductComponent } from './components/company-product/company-product.component';
import { UnitTypesComponent } from './components/unit-types/unit-types.component';
import { UnitTypeComponent } from './components/unit-type/unit-type.component';
import { AdminCommerceComponent } from './components/admin-commerce/admin-commerce.component';
import { OptionsComponent } from './components/options/options.component';
import { OptionProductStorageComponent } from './components/options/option-product-storage/option-product-storage.component';
// tslint:disable-next-line:max-line-length
import { OptionExtraDataProductTypeComponent } from './components/options/option-extra-data-product-type/option-extra-data-product-type.component';
import { StockComponent } from './components/stock/stock.component';
import { StockTypesComponent } from './components/stock-types/stock-types.component';
import { StockTypeComponent } from './components/stock-type/stock-type.component';
import { ProductTypeComponent } from './components/product-type/product-type.component';
import { ProductTypesComponent } from './components/product-types/product-types.component';
import { SaleComponent } from './components/sale/sale.component';


const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent},
  {path: 'main', component: LoggedComponent, canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
      {path: 'suppliers', component: SuppliersComponent, canActivate: [AuthGuard]},
      {path: 'supplier', component: SupplierComponent, canActivate: [AuthGuard]},
      {path: 'supplier/:id', component: SupplierComponent, canActivate: [AuthGuard]},
      {path: 'commerces', component: CommercesComponent, canActivate: [AuthGuard]},
      {path: 'commerce', component: CommerceComponent, canActivate: [AuthGuard]},
      {path: 'admin-commerce', component: AdminCommerceComponent, canActivate: [AuthGuard]},
      {path: 'admin-commerce/:id', component: AdminCommerceComponent, canActivate: [AuthGuard]},
      {path: 'branches', component: BranchesComponent, canActivate: [AuthGuard]},
      {path: 'branches/commerce/:id', component: BranchesComponent, canActivate: [AuthGuard]},
      {path: 'branch', component: BranchComponent, canActivate: [AuthGuard]},
      {path: 'branch/:id', component: BranchComponent, canActivate: [AuthGuard]},
      {path: 'branch/commerce/:commerceId', component: BranchComponent, canActivate: [AuthGuard]},
      {path: 'branch/:id/commerce/:commerceId', component: BranchComponent, canActivate: [AuthGuard]},
      {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
      {path: 'users/commerce/:id', component: UsersComponent, canActivate: [AuthGuard]},
      {path: 'user/:id', component: UserComponent, canActivate: [AuthGuard]},
      {path: 'user', component: UserComponent, canActivate: [AuthGuard]},
      {path: 'user/:id/commerce/:commerceId', component: UserComponent, canActivate: [AuthGuard]},
      {path: 'user/commerce/:commerceId', component: UserComponent, canActivate: [AuthGuard]},
      {path: 'products', component: ProductsComponent, canActivate: [AuthGuard]},
      {path: 'product', component: ProductComponent, canActivate: [AuthGuard]},
      {path: 'product/:id', component: ProductComponent, canActivate: [AuthGuard]},
      {path: 'product/:productId/stock', component: StockComponent, canActivate: [AuthGuard]},
      {path: 'company-products', component: CompanyProductsComponent, canActivate: [AuthGuard]},
      {path: 'company-products/product/:productId', component: CompanyProductsComponent, canActivate: [AuthGuard]},
      {path: 'company-product', component: CompanyProductComponent, canActivate: [AuthGuard]},
      {path: 'company-product/product/:productId', component: CompanyProductComponent, canActivate: [AuthGuard]},
      {path: 'company-product/:id', component: CompanyProductComponent, canActivate: [AuthGuard]},
      {path: 'company-product/:id/product/:productId', component: CompanyProductComponent, canActivate: [AuthGuard]},
      {path: 'unit-types', component: UnitTypesComponent, canActivate: [AuthGuard]},
      {path: 'unit-types/product/:productId', component: UnitTypesComponent, canActivate: [AuthGuard]},
      {path: 'unit-type', component: UnitTypeComponent, canActivate: [AuthGuard]},
      {path: 'unit-type/product/:productId', component: UnitTypeComponent, canActivate: [AuthGuard]},
      {path: 'unit-type/:id', component: UnitTypeComponent, canActivate: [AuthGuard]},
      {path: 'unit-type/:id/product/:productId', component: UnitTypeComponent, canActivate: [AuthGuard]},
      {path: 'options', component: OptionsComponent, canActivate: [AuthGuard]},
      {path: 'options/product-storage', component: OptionProductStorageComponent, canActivate: [AuthGuard]},
      {path: 'options/extra-data-product-type', component: OptionExtraDataProductTypeComponent, canActivate: [AuthGuard]},
      {path: 'stock-types/product/:productId', component: StockTypesComponent, canActivate: [AuthGuard]},
      {path: 'stock-type/product/:productId', component: StockTypeComponent, canActivate: [AuthGuard]},
      {path: 'stock-type/:id/product/:productId', component: StockTypeComponent, canActivate: [AuthGuard]},
      {path: 'product-types', component: ProductTypesComponent, canActivate: [AuthGuard]},
      {path: 'product-types/product/:productId', component: ProductTypesComponent, canActivate: [AuthGuard]},
      {path: 'product-type', component: ProductTypeComponent, canActivate: [AuthGuard]},
      {path: 'product-type/product/:productId', component: ProductTypeComponent, canActivate: [AuthGuard]},
      {path: 'product-type/:id', component: ProductTypeComponent, canActivate: [AuthGuard]},
      {path: 'product-type/:id/product/:productId', component: ProductTypeComponent, canActivate: [AuthGuard]},
      {path: 'sale', component: SaleComponent, canActivate: [AuthGuard]},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
