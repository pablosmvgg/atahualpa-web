import { environment } from '../../environments/environment';

export class ErrorMessagesUtil {

  constructor() {

  }

  getErrorMessage(element, name) {
    if (element.hasError('required')) {
      return name + environment.error_required;
    }
    if (element.hasError('email')) {
      return name + environment.error_email_invalid;
    }
  }

}
