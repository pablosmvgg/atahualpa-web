import { environment } from '../../environments/environment';
import { UserRole } from '../modules/user-role';

export class RoleUtil {

  constructor() {

  }

  validateCommerceRole(userRoles: UserRole[]) {
    for (let i = 0; i < userRoles.length; i++) {
      const userRole = userRoles[i];
      if (userRole.id === environment.role_commerce) {
        return true;
      }
    }
    return false;
  }

  validateAdminRole(userRoles: UserRole[]) {
    for (let i = 0; i < userRoles.length; i++) {
      const userRole = userRoles[i];
      if (userRole.id === environment.role_admin) {
        return true;
      }
    }
    return false;
  }

}
