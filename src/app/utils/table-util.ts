import { Sort, MatTableDataSource } from '@angular/material';

export class TableUtil {

   public dataSource: MatTableDataSource<any>;
   public elements: any[] = [];

   public pageSize = 10;
   public pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(dataSource: MatTableDataSource<any>, elements: any[]) {
    this.dataSource = dataSource;
    this.elements = elements;
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  sortData(sort: Sort) {
    const data = this.elements.slice();

    this.elements = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.name, b.name, isAsc);
        case 'address': return this.compare(a.address, b.address, isAsc);

        default: return 0;
      }
    });
    this.dataSource.data = this.elements;
  }

}
