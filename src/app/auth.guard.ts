import { User } from './modules/user';
import { UserRole } from './modules/user-role';
import { UserRoleService } from './services/user-role.service';
import { UserService } from './services/user.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RoleUtil } from './utils/role-util';

@Injectable()
export class AuthGuard implements CanActivate {

  private userRoles: UserRole[];
  private user: User;

  constructor(public userService: UserService,
    public userRoleService: UserRoleService,
    public router: Router,
    public roleUtil: RoleUtil) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // TODO: con esta solucion anda pero busca todo el tiempo
      // los roles, ver si esta bien o se puede mejorar
      if (!this.userRoles) {
        this.userRoleService.getRolesForLoggedUser()
          .subscribe(response => {
            this.userRoles = response;
            if (this.validateToken()) {
              this.executeValidateCommerceRole(state);
              this.executeValidateAdminRole(state);
            }
        });
      }
      else {
        if (this.validateToken()) {
            this.executeValidateCommerceRole(state);
            this.executeValidateAdminRole(state);
        }
      }

    return true;
  }

  executeValidateCommerceRole(state: RouterStateSnapshot) {
    if (state.url.startsWith('/main/dashboard') ||
      state.url.startsWith('/main/suppliers') ||
      state.url.startsWith('/main/supplier') ||
      state.url.startsWith('/main/commerce') ||
      state.url.startsWith('/main/branches') ||
      state.url.startsWith('/main/branch') ||
      state.url.startsWith('/main/users') ||
      state.url.startsWith('/main/user') ||
      state.url.startsWith('/main/products') ||
      state.url.startsWith('/main/product') ||
      state.url.startsWith('/main/company-products') ||
      state.url.startsWith('/main/company-product') ||
      state.url.startsWith('/main/unit-types') ||
      state.url.startsWith('/main/unit-type') ||
      state.url.startsWith('/main/options') ||
      state.url.startsWith('/main/stock-types') ||
      state.url.startsWith('/main/stock-type') ||
      state.url.startsWith('/main/product-types') ||
      state.url.startsWith('/main/product-type')) {

        return this.roleUtil.validateCommerceRole(this.userRoles);
    }
  }

  executeValidateAdminRole(state: RouterStateSnapshot) {
    if (state.url.startsWith('/main/commerces') ||
      state.url.startsWith('/main/admin-commerce')) {
      return this.roleUtil.validateAdminRole(this.userRoles);
    }
  }

  validateToken() {
    const token = sessionStorage.getItem('userToken');
    if (!token) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }


}
