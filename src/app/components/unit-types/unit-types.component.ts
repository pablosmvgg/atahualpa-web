import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {Router} from '@angular/router';
import { UnitType } from '../../modules/unit-type';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatPaginator, Sort} from '@angular/material';
import { UnitTypeService } from '../../services/unit-type.service';
import { ActivatedRoute } from '@angular/router';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-unit-type-confirm-dialog',
  templateUrl: 'delete-unit-type-confirm-dialog.html',
})
export class DeleteUnitTypeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteUnitTypeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public unitType: UnitType
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-unit-types',
  templateUrl: './unit-types.component.html',
  styleUrls: ['./unit-types.component.css']
})
export class UnitTypesComponent implements OnInit {

  public unitTypes: UnitType[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public productId: number;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private unitTypeService: UnitTypeService,
    private dialog: MatDialog) {
      this.displayedColumns = ['name', 'options'];
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    this.getUnitTypes();
  }

  getUnitTypes(): void {
    this.unitTypeService.getUnitTypes()
      .subscribe(unitTypes => {
        this.unitTypes = unitTypes.slice();
        this.dataSource = new MatTableDataSource(this.unitTypes);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.unitTypes);
      });
  }

  goToUnitType(): void {
    if (this.productId) {
      this.router.navigate(['main/unit-type/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/unit-type']);
    }
  }

  editUnitType(unitType): void {
    if (this.productId) {
      this.router.navigate(['main/unit-type/' + unitType.id + '/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/unit-type/' + unitType.id]);
    }

  }

  deleteUnitType(unitType: UnitType): void {
    this.openDialog(unitType);
  }

  openDialog(unitType: UnitType): void {
    const dialogRef = this.dialog.open(DeleteUnitTypeDialogComponent, {
      width: '250px',
      data: unitType
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.unitTypeService.deleteUnitType(unitType.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.unitTypes.indexOf(unitType, 0);
              if (index > -1) {
                this.unitTypes.splice(index, 1);
                this.dataSource.data = this.unitTypes;
              }
            }
          });
      }
    });
  }

  returnProduct() {
    if (this.productId) {
      this.router.navigate(['main/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product']);
    }
  }

}
