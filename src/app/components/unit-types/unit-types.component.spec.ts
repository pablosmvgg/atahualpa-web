import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitTypesComponent } from './unit-types.component';

describe('UnitTypesComponent', () => {
  let component: UnitTypesComponent;
  let fixture: ComponentFixture<UnitTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
