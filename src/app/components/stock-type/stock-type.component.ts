import { Component, OnInit } from '@angular/core';
import { ProductStockType } from '../../modules/product-stock-type';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductStockTypeService } from '../../services/product-stock-type.service';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-stock-type',
  templateUrl: './stock-type.component.html',
  styleUrls: ['./stock-type.component.css']
})
export class StockTypeComponent implements OnInit {

  public stockType: ProductStockType;
  public stockTypeForm: FormGroup;
  public productId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private stockTypeService: ProductStockTypeService,
    public errorMessagesUtil: ErrorMessagesUtil
  ) {

    this.createForm();
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    if (this.route.snapshot.params.id) {
      this.stockTypeService.getProductStockType(this.route.snapshot.params.id)
        .subscribe(response => {
          this.stockType = response;
          this.fillDataForm();
        });
    }
    else {
      this.stockType = new ProductStockType();
    }
  }

  createForm() {
    this.stockTypeForm = this.formBuilder.group({
      name: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.stockTypeForm.get('name').setValue(this.stockType.name);
  }

  onSubmitUnitType() {
    this.stockType.name = this.stockTypeForm.value.name;


    this.stockTypeService.saveProductStockType(this.stockType)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.returnToStockTypes();
        }
     });
  }

  cancelStockType() {
    this.returnToStockTypes();
  }

  returnToStockTypes() {
    this.router.navigate(['main/stock-types/product/' + this.productId]);
  }

}
