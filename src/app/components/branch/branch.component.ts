import { BranchCommerce } from '../../modules/branch-commerce';
import { BranchCommerceService } from '../../services/branch.commerce.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  public branchCommerce: BranchCommerce;
  public branchCommerceForm: FormGroup;
  public commerceId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private branchCommerceService: BranchCommerceService,
    public errorMessagesUtil: ErrorMessagesUtil
  ) {

    this.createForm();
  }

  ngOnInit() {
    if (this.route.snapshot.params.id) {
      this.branchCommerceService.getBranchCommerce(this.route.snapshot.params.id)
        .subscribe(response => {
          this.branchCommerce = response;
          this.fillDataForm();
        });
    }
    else {
      this.branchCommerce = new BranchCommerce();
    }
    if (this.route.snapshot.params.commerceId) {
      this.commerceId = this.route.snapshot.params.commerceId;
    }
  }

  createForm() {
    this.branchCommerceForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      province: ['', Validators.required],
      city: ['', Validators.required],
      cityCode: ['', Validators.required],
      phone: ['', []],
      cellphone: ['', []],
      email: ['', [Validators.email]]
    });
  }

  fillDataForm() {
    this.branchCommerceForm.get('name').setValue(this.branchCommerce.name);
    this.branchCommerceForm.get('address').setValue(this.branchCommerce.address);
    this.branchCommerceForm.get('country').setValue(this.branchCommerce.country);
    this.branchCommerceForm.get('province').setValue(this.branchCommerce.province);
    this.branchCommerceForm.get('city').setValue(this.branchCommerce.city);
    this.branchCommerceForm.get('cityCode').setValue(this.branchCommerce.cityCode);
    this.branchCommerceForm.get('phone').setValue(this.branchCommerce.phone);
    this.branchCommerceForm.get('cellphone').setValue(this.branchCommerce.cellphone);
    this.branchCommerceForm.get('email').setValue(this.branchCommerce.email);
  }

  onSubmitBranchCommerce() {
    this.branchCommerce.name = this.branchCommerceForm.value.name;
    this.branchCommerce.address = this.branchCommerceForm.value.address;
    this.branchCommerce.city = this.branchCommerceForm.value.city;
    this.branchCommerce.cityCode = this.branchCommerceForm.value.cityCode;
    this.branchCommerce.province = this.branchCommerceForm.value.province;
    this.branchCommerce.country = this.branchCommerceForm.value.country;
    this.branchCommerce.phone = this.branchCommerceForm.value.phone;
    this.branchCommerce.cellphone = this.branchCommerceForm.value.cellphone;
    this.branchCommerce.email = this.branchCommerceForm.value.email;

    this.branchCommerceService.saveBranchCommerce(this.branchCommerce, this.commerceId)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.redirectToBranches();
        }
     });
  }

  cancelProduct() {
    this.redirectToBranches();
  }

  redirectToBranches() {
    if (this.commerceId) {
      this.router.navigate(['main/branches/commerce/' + this.commerceId]);
    }
    // tslint:disable-next-line:one-line
    else {
      this.router.navigate(['main/branches']);
    }
  }

}
