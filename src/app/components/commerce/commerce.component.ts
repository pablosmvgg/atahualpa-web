import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CommerceService} from '../../services/commerce.service';
import {Commerce} from '../../modules/commerce';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-commerce',
  templateUrl: './commerce.component.html',
  styleUrls: ['./commerce.component.css']
})
export class CommerceComponent implements OnInit {

  public commerce: Commerce;
  public commerceForm: FormGroup;
  public commerceValidationMessages;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private commerceService: CommerceService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.commerceValidationMessages = {
      'required': ' es requerido',
      'email': ' no es un email válido'
    };
    this.createForm();
  }

  ngOnInit() {
    this.commerceService.getCommerce()
      .subscribe(response => {
        this.commerce = response;
        this.fillDataForm();
      });
  }

  getErrorMessage(element, name) {
    if (element.hasError('required')) {
      return name + this.commerceValidationMessages.required;
    }
  }

  createForm() {
    this.commerceForm = this.fb.group({
      name: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.commerceForm.get('name').setValue(this.commerce.name);
  }

  onSubmitSupplier(commerceForm) {
    this.commerce.name = commerceForm.name;
    this.commerceService.saveCommerce(this.commerce)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.router.navigate(['main/dashboard']);
        }
      });
  }
  cancelCommerce() {
    this.router.navigate(['main/dashboard']);
  }

  goToBranches() {
    this.router.navigate(['main/branches']);
  }

  goToUsers() {
    this.router.navigate(['main/users']);
  }

}
