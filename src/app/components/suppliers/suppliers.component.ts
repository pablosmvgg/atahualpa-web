import {Supplier} from '../../modules/supplier';
import {SupplierService} from '../../services/supplier.service';
import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Sort} from '@angular/material/sort';
import {PageEvent} from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-supplier-confirm-dialog',
  templateUrl: 'delete-supplier-confirm-dialog.html',
})
export class DeleteSupplierDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteSupplierDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public supplier: Supplier
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  public suppliers: Supplier[];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  constructor(
    private router: Router,
    private supplierService: SupplierService,
    private dialog: MatDialog
  ) {
    this.displayedColumns = ['name', 'address', 'phone', 'cellphone', 'email', 'options'];
  }

  ngOnInit() {
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers()
      .subscribe(suppliers => {
        this.suppliers = suppliers.slice();
        this.dataSource = new MatTableDataSource(this.suppliers);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.suppliers);
      });

  }

  goToSupplier(): void {
    this.router.navigate(['main/supplier']);
  }

  editSupplier(supplier): void {
    this.router.navigate(['main/supplier/' + supplier.id]);
  }

  deleteSupplier(supplier: Supplier): void {
    this.openDialog(supplier);
  }

  openDialog(supplier: Supplier): void {
    const dialogRef = this.dialog.open(DeleteSupplierDialogComponent, {
      width: '250px',
      data: supplier
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.supplierService.deleteSupplier(supplier.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.suppliers.indexOf(supplier, 0);
              if (index > -1) {
                this.suppliers.splice(index, 1);
                this.dataSource.data = this.suppliers;
              }
            }
          });
      }
    });
  }

}


