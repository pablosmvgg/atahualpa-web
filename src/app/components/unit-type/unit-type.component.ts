import { UnitType } from '../../modules/unit-type';
import { UnitTypeService } from '../../services/unit-type.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-unit-type',
  templateUrl: './unit-type.component.html',
  styleUrls: ['./unit-type.component.css']
})
export class UnitTypeComponent implements OnInit {

  public unitType: UnitType;
  public unitTypeForm: FormGroup;
  public productId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private unitTypeService: UnitTypeService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.createForm();
}

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    if (this.route.snapshot.params.id) {
      this.unitTypeService.getUnitType(this.route.snapshot.params.id)
        .subscribe(response => {
          this.unitType = response;
          this.fillDataForm();
        });
    }
    else {
      this.unitType = new UnitType();
    }
  }

  createForm() {
    this.unitTypeForm = this.formBuilder.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.unitTypeForm.get('name').setValue(this.unitType.name);
    this.unitTypeForm.get('shortName').setValue(this.unitType.shortName);
  }

  onSubmitUnitType() {
    this.unitType.name = this.unitTypeForm.value.name;
    this.unitType.shortName = this.unitTypeForm.value.shortName;

    this.unitTypeService.saveUnitType(this.unitType)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.returnToUnitTypes();
        }
     });
  }

  cancelUnitType() {
    this.returnToUnitTypes();
  }

  returnToUnitTypes() {
    if (this.productId) {
      this.router.navigate(['main/unit-types/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/unit-types']);
    }
  }

}
