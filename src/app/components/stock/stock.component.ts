import { Component, OnInit } from '@angular/core';
import { ProductStock } from '../../modules/product-stock';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductStockService } from '../../services/product-stock.service';
import { BranchCommerce } from '../../modules/branch-commerce';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';
import { ProductStockTypeService } from '../../services/product-stock-type.service';
import { ProductStockType } from '../../modules/product-stock-type';
import { TemplateService } from '../../services/templates/template.service';
import { Template } from '../../modules/templates/template';
import { TemplateValue } from '../../modules/templates/template-value';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

  public productStocks: ProductStock[];
  public branchCommerces: BranchCommerce[];
  public productStockTypes: ProductStockType[];
  public stockForm: FormGroup;
  public stockTitle: string;
  public stockTitlePart2: string;
  public templateTypeId = 1;
  public templateStorage: Template;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productStockService: ProductStockService,
    private productStockTypeServiceService: ProductStockTypeService,
    private templateService: TemplateService,
    public errorMessagesUtil: ErrorMessagesUtil) {

      this.productStocks = [];
      this.fillForm();
  }

  ngOnInit() {
    // https://github.com/Gbuomprisco/ngx-chips
    this.getProductStockTypes();
    this.getStorageTemplate();

  }

  getProductStockTypes() {

    this.productStockTypeServiceService.getProductStockTypes()
    .subscribe(response => {
        this.productStockTypes = response;
    });
  }

  getStorageTemplate() {

    this.templateService.getTemplateByType(this.templateTypeId)
      .subscribe(templates => {
        if (templates && templates.length > 0) {
          this.templateStorage = templates[0];
        }
        const retrievedStock = localStorage.getItem('productStocksTemp');
        if (retrievedStock) {
          this.getProductStocksTempLocal(retrievedStock);
        }
        else {
          this.getProductStocks();
        }
    });
  }

  getProductStocksTempLocal(retrievedStock) {
    this.productStocks = JSON.parse(retrievedStock);
    localStorage.removeItem('productStocksTemp');
    this.fillForm();
  }

  saveProductStocksTempLocal() {
    this.fillProductStocks();
    localStorage.setItem('productStocksTemp', JSON.stringify(this.productStocks));
  }

  getProductStocks() {
    this.productStockService.getProductStocks(this.route.snapshot.params.productId)
    .subscribe(response => {
        this.productStocks = response;
        this.fillForm();
    });
  }

  fillForm() {
    const elements = [];

    this.productStocks.forEach(productStock => {
      if (this.productStocks.length > 0) {
        this.stockTitle = productStock.product.name;
        this.stockTitlePart2 = productStock.product.companyProduct.name;
      }
      elements.push(this.createValueElement(productStock));
    });

    this.stockForm = this.formBuilder.group({
        productStocks: this.formBuilder.array(elements)
    });

  }

  createValueElement(productStock: ProductStock): FormGroup {

    const storageValues = [];
    if (this.templateStorage) {
      this.templateStorage.templateElements.forEach(templateElement => {

        const storeValueFound: TemplateValue = productStock.storageValues.find(storageValue => {
            return storageValue.templateElement.id === templateElement.id;
        });
        const element = this.formBuilder.group({
            elementName: [templateElement.name, Validators.required],
            elementType: [templateElement.templateValueType.id, Validators.required],
            elementValue: [storeValueFound ? storeValueFound.valuez : '', Validators.required]
        });
        storageValues.push(element);
      });
    }

    return this.formBuilder.group({
      id: productStock.id,
      branchCommerceId: [productStock.branchCommerce.id, Validators.required],
      branchCommerceName: [productStock.branchCommerce.name, Validators.required],
      stock: [productStock.quantity, Validators.required],
      stockType: [productStock.productStockType ? productStock.productStockType.id : null, Validators.required],
      storageValues: this.formBuilder.array(storageValues)
    });
  }

  fillProductStocks() {
    const productStocksToSave = this.stockForm.get('productStocks').value;
    for ( let i = 0; i < productStocksToSave.length; i++) {
        if (productStocksToSave[i].id) {
            this.productStocks[i].id = productStocksToSave[i].id;
        }
        this.productStocks[i].quantity = productStocksToSave[i].stock;
        const stockType = this.productStockTypes.find(stockTypeFound => {
            return stockTypeFound.id === productStocksToSave[i].stockType;
        });
        this.productStocks[i].productStockType = stockType;
        const storageValues = [];
        for (let j = 0; j < productStocksToSave[i].storageValues.length; j++) {
          const valuez = productStocksToSave[i].storageValues[j].elementValue;
          const templateElement = this.templateStorage.templateElements[j];
          const templateValue = new TemplateValue(valuez instanceof Array ? valuez : [valuez], templateElement);
          if (this.productStocks[i].storageValues) {
          const storageValueFound = this.productStocks[i].storageValues.find(storageValue => {
            return storageValue.templateElement.id === templateElement.id;
          });
          if (storageValueFound) {
            templateValue.id = storageValueFound.id;
          }
        }
        storageValues.push(templateValue);
      }
      this.productStocks[i].storageValues = storageValues;
    }
  }

  onSubmitProductStock() {

    this.fillProductStocks();

    this.productStockService.saveProductStocks(this.productStocks)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.goToProducts();
        }
     });
  }

  goToProductStockTypes() {
    this.saveProductStocksTempLocal();
    this.router.navigate(['main/stock-types/product/' + this.route.snapshot.params.productId]);
  }

  goToProductStockType() {
    this.saveProductStocksTempLocal();
    this.router.navigate(['main/stock-type/product/' + this.route.snapshot.params.productId]);
  }

  goToProducts() {
    this.router.navigate(['main/products']);
  }

  getFormProductStocks() {
      return <FormArray>this.stockForm.get('productStocks');
  }

  getStorageValues(productStock) {
    return <FormArray>productStock.get('storageValues');
  }

}
