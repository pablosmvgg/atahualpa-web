
import {throwError as observableThrowError } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Menu } from '../../modules/menu';
import { MenuItem } from '../../modules/menu-item';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { UserRoleService } from '../../services/user-role.service';
import { UserRole } from '../../modules/user-role';
import { RoleUtil } from '../../utils/role-util';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})
export class MenuBarComponent implements OnInit {

  public menus: Menu[] = [];

  private isCommerceOwner: boolean;
  private isAdminOwner: boolean;

  constructor(
    private router: Router,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private roleUtil: RoleUtil) {

  }

  private userRoles: UserRole[];

  ngOnInit() {

    this.userRoleService.getRolesForLoggedUser()
      .subscribe(response => {
        this.userRoles = response;
        this.isCommerceOwner = this.roleUtil.validateCommerceRole(this.userRoles);
        this.isAdminOwner = this.roleUtil.validateAdminRole(this.userRoles);
        this.generateMenuItemsByRole();
      });
  }

  generateMenuItemsByRole() {

    const menuItem11 = new MenuItem('Escritorio', 'main/dashboard', false);
    const menuItem12 = new MenuItem('Opciones', 'main/options', false);
    const menuItem13 = new MenuItem('Cerrar Sesion', 'main/logout', true);

    menuItem13.action = this.logout;
    menuItem13.userService = this.userService;
    menuItem13.router = this.router;
    const menu1Array = [menuItem11, menuItem12, menuItem13];
    const menu1 = new Menu('Principal', menu1Array);
    this.menus.push(menu1);

    if (this.isAdminOwner) {
      const menuItem21 = new MenuItem('Comercios', 'main/commerces', false);
      const menu2Array = [menuItem21];
      const menu2 = new Menu('Comercio', menu2Array);
      this.menus.push(menu2);
    }
    else {
      if (this.isCommerceOwner) {
        const menuItem21 = new MenuItem('Proveedores', 'main/suppliers', false);
        const menuItem22 = new MenuItem('Compras', 'main/purchases', false);
        const menu2Array = [menuItem21, menuItem22];
        const menu2 = new Menu('Compras', menu2Array);
        this.menus.push(menu2);
      }

      const menuItem31 = new MenuItem('Ventas', 'main/sale', false);
      const menuItem32 = new MenuItem('A Definir', 'main/purchases', false);
      const menu3Array = [menuItem31, menuItem32];
      const menu3 = new Menu('Ventas', menu3Array);
      this.menus.push(menu3);

      if (this.isCommerceOwner) {
        const menuItem41 = new MenuItem('Productos', 'main/products', false);
        const menuItem42 = new MenuItem('Stock', 'main/stocks', false);
        const menu4Array = [menuItem41, menuItem42];
        const menu4 = new Menu('Stock', menu4Array);
        this.menus.push(menu4);
      }

      if (this.isCommerceOwner) {
        const menuItem51 = new MenuItem('Comercio', 'main/commerce', false);
        const menuItem52 = new MenuItem('Sucursales', 'main/branches', false);
        const menuItem53 = new MenuItem('Usuarios', 'main/users', false);
        const menu5Array = [menuItem51, menuItem52, menuItem53];
        const menu5 = new Menu('Negocio', menu5Array);
        this.menus.push(menu5);
      }
    }
  }

  showHideMenu() {
    if (sessionStorage.getItem('userToken') === null) {
      return true;
    }
    return false;
  }

  logout() {
    this.userService.logout().subscribe(
      data => {
         sessionStorage.removeItem('userToken');
         this.router.navigate(['login']);
      },
      error => {
       console.error('Error!' + error);
       return observableThrowError(error);
     });
  }
}
