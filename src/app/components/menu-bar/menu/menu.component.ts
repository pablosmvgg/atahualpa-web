import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MenuItem } from '../../../modules/menu-item';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() name: string;
  @Input() items: MenuItem[];

  constructor(private router: Router,) { }

  @ViewChild(MatMenuTrigger, {static: false}) trigger: MatMenuTrigger;

  ngOnInit() {

  }

  closeMyMenu() {
    this.trigger.closeMenu();
  }

  goToLocation(item) {
	localStorage.clear();
    if (item.isAction) {
      item.action();
    } else {
      this.router.navigate([item.url]);
    }
  }
  
  hideMenu() {
    return this.items.length === 0;
  }

}
