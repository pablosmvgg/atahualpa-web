import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {Router} from '@angular/router';
import { Product } from '../../modules/product';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatPaginator, Sort} from '@angular/material';
import { ProductService } from '../../services/product.service';
import { MessageService } from '../../services/message.service';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-product-confirm-dialog',
  templateUrl: 'delete-product-confirm-dialog.html',
})
export class DeleteProductDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public product: Product
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Product[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private productService: ProductService,
    private dialog: MatDialog,
    private messageService: MessageService) {
      this.displayedColumns = ['name', 'productType', 'unit', 'companyProductName', 'options'];
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(products => {
        this.products = products.slice();
        this.dataSource = new MatTableDataSource(this.products);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.products);
      });
  }

  goToProduct(): void {
    this.router.navigate(['main/product']);
  }

  editProduct(product): void {
    this.router.navigate(['main/product/' + product.id]);
  }

  editProductStock(product: Product) {
    this.router.navigate(['main/product/' + product.id + '/stock']);
  }

  deleteProduct(product: Product): void {
    this.openDialog(product);
  }

  openDialog(product: Product): void {
    const dialogRef = this.dialog.open(DeleteProductDialogComponent, {
      width: '250px',
      data: product
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.productService.deleteProduct(product.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.products.indexOf(product, 0);
              if (index > -1) {
                this.products.splice(index, 1);
                this.dataSource.data = this.products;
              }
            }
          });
      }
    });
  }

}
