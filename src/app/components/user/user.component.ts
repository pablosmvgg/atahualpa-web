import {User} from '../../modules/user';
import {UserRole} from '../../modules/user-role';
import {BranchCommerce} from '../../modules/branch-commerce';
import {UserService} from '../../services/user.service';
import {BranchCommerceService} from '../../services/branch.commerce.service';
import {UserRoleService} from '../../services/user-role.service';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user: User;
  public userForm: FormGroup;
  public userRoles: UserRole[];
  public branchCommerces: BranchCommerce[];
  public commerceId: number;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private branchCommerceService: BranchCommerceService,
    public errorMessagesUtil: ErrorMessagesUtil
  ) {
      this.createForm();
  }

  ngOnInit() {
    if (this.route.snapshot.params.commerceId) {
      this.commerceId = this.route.snapshot.params.commerceId;
    }
    this.getUserRoles();
    this.getBranchCommerces();
    if (this.route.snapshot.params.id) {
    this.userService.getUserByCommerce(this.route.snapshot.params.id)
    .subscribe(response => {
      this.user = response;
      this.fillDataForm();
      });
    }
    else {
      this.user = new User();
    }
  }

  getUserRoles() {
    this.userRoleService.getUserCommerceRoles()
      .subscribe(response => {
        this.userRoles = response;
    });
  }

  getBranchCommerces() {
    this.branchCommerceService.getBranchCommerces(this.commerceId)
    .subscribe(response => {
      this.branchCommerces = response;
    });
  }

  createForm() {

    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      userRoles: [null, Validators.required],
      branchCommerces: [null, Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  fillDataForm() {
    const branchCommerceIds = [];
    if (this.user.branchCommerces) {
      for (const branch of this.user.branchCommerces) {
        branchCommerceIds.push(branch.id);
      }
    }
    this.userForm.get('username').setValue(this.user.username);
    this.userForm.get('password').setValue(this.user.password);
    this.userForm.get('userRoles').setValue(this.user.userRoles ? this.user.userRoles[0].id : 0);
    this.userForm.get('branchCommerces').setValue(branchCommerceIds);
    this.userForm.get('name').setValue(this.user.name);
    this.userForm.get('email').setValue(this.user.email);

  }

  onSubmitUser(userForm) {
    this.user.username = userForm.username;
    this.user.password = userForm.password;
    const userRole = this.userRoles.find(userRoleFound => {
      return userRoleFound.id === userForm.userRoles;
    });
    this.user.userRoles = [userRole];
    const branchCommerces = this.branchCommerces.filter(branchCommerce => {
      let branchFound = false;
      userForm.branchCommerces.forEach(branchSelected => {
        if (branchCommerce.id === branchSelected) {
          branchFound = true;
        }
      });
      return branchFound;
    });
    this.user.branchCommerces = branchCommerces;
    this.user.name = userForm.name;
    this.user.email = userForm.email;

    this.userService.saveUser(this.user, this.commerceId)
    .subscribe(response => {

      if (response.status === 200 || response.status === 201) {
        this.redirectToUsers();
      }
    });
  }

  cancelUser() {
    this.redirectToUsers();
  }

  redirectToUsers() {
    if (this.commerceId) {
      this.router.navigate(['main/users/commerce/' + this.commerceId]);
    }
    else {
      this.router.navigate(['main/users']);
    }
  }
}
