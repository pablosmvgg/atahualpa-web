import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatPaginator} from '@angular/material';
import { BranchCommerce } from '../../modules/branch-commerce';
import {Router} from '@angular/router';
import {BranchCommerceService} from '../../services/branch.commerce.service';
import {Message} from '../../modules/message';
import {MessageService} from '../../services/message.service';
import { MessageType } from '../../modules/message-type';
import { ActivatedRoute } from '@angular/router';
import { TableUtil } from '../../utils/table-util';


@Component({
    selector: 'app-delete-branch-confirm-dialog',
    templateUrl: 'delete-branch-confirm-dialog.html',
})
export class DeleteBranchDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteBranchDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public branchCommerce: BranchCommerce
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {

  public branchCommerces: BranchCommerce[];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public commerceId: number;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private branchCommerceService: BranchCommerceService,
    private dialog: MatDialog,
    private messageService: MessageService) {
      this.displayedColumns = ['name', 'address', 'options'];

  }

  ngOnInit() {
    this.getBranches();
  }

  getBranches(): void {

    this.commerceId = this.route.snapshot.params.id;
    this.branchCommerceService.getBranchCommerces(this.commerceId)
      .subscribe(branchCommerces => {
        this.branchCommerces = branchCommerces.slice();
        this.dataSource = new MatTableDataSource(this.branchCommerces);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.branchCommerces);
    });
  }

  goToBranchCommerce(): void {
    if (this.commerceId) {
      this.router.navigate(['main/branch/commerce/' + this.commerceId]);
    }
    else {
      this.router.navigate(['main/branch']);
    }
  }

  editBranchCommerce(branchCommerce): void {
    if (this.commerceId) {
      this.router.navigate(['main/branch/' + branchCommerce.id + '/commerce/' + this.commerceId]);
    }
    else {
      this.router.navigate(['main/branch/' + branchCommerce.id]);
    }
  }

  deleteBranchCommerce(branchCommerce: BranchCommerce): void {
    if (this.branchCommerces.length > 1) {
      this.openDialog(branchCommerce);
    }
    else {
      const message = new Message('El comercio tiene que tener al menos una sucursal.', MessageType.Error);
      this.messageService.add(message);
    }
  }

  openDialog(branchCommerce: BranchCommerce): void {
    const dialogRef = this.dialog.open(DeleteBranchDialogComponent, {
      width: '250px',
      data: branchCommerce
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.branchCommerceService.deleteBranchCommerce(branchCommerce.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.branchCommerces.indexOf(branchCommerce, 0);
              if (index > -1) {
                this.branchCommerces.splice(index, 1);
                this.dataSource.data = this.branchCommerces;
              }
            }
          });
      }
    });
  }

  returnCommerces() {
    this.router.navigate(['main/commerces']);
  }

}
