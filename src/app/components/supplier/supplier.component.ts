import {Supplier} from '../../modules/supplier';
import {SupplierService} from '../../services/supplier.service';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

  public supplier: Supplier;
  public supplierForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private supplierService: SupplierService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.createForm();
  }
  // https://stackblitz.com/edit/angular-hcr8bl-rnayva?file=app%2Finput-overview-example.ts

  ngOnInit() {
    if (this.route.snapshot.params.id) {
      this.supplierService.getSupplier(this.route.snapshot.params.id)
        .subscribe(response => {
          this.supplier = response;
          this.fillDataForm();
        });
    }
    else {
      this.supplier = new Supplier();
    }
  }

  createForm() {
    this.supplierForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      province: ['', Validators.required],
      city: ['', Validators.required],
      cityCode: ['', Validators.required],
      phone: ['', []],
      cellphone: ['', []],
      email: ['', [Validators.email]]
    });
  }

  fillDataForm() {
    this.supplierForm.get('name').setValue(this.supplier.name);
    this.supplierForm.get('address').setValue(this.supplier.address);
    this.supplierForm.get('country').setValue(this.supplier.country);
    this.supplierForm.get('province').setValue(this.supplier.province);
    this.supplierForm.get('city').setValue(this.supplier.city);
    this.supplierForm.get('cityCode').setValue(this.supplier.cityCode);
    this.supplierForm.get('phone').setValue(this.supplier.phone);
    this.supplierForm.get('cellphone').setValue(this.supplier.cellphone);
    this.supplierForm.get('email').setValue(this.supplier.email);
  }

  onSubmitSupplier(supplierForm) {
    this.supplier.name = supplierForm.name;
    this.supplier.address = supplierForm.address;
    this.supplier.city = supplierForm.city;
    this.supplier.cityCode = supplierForm.cityCode;
    this.supplier.province = supplierForm.province;
    this.supplier.country = supplierForm.country;
    this.supplier.phone = supplierForm.phone;
    this.supplier.cellphone = supplierForm.cellphone;
    this.supplier.email = supplierForm.email;

    this.supplierService.saveSupplier(this.supplier)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.router.navigate(['main/suppliers']);
        }
     });
  }

  cancelSupplier() {
    this.router.navigate(['main/suppliers']);
  }

}
