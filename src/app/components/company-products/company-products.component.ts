import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {Router} from '@angular/router';
import { CompanyProduct } from '../../modules/company-product';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatPaginator, Sort} from '@angular/material';
import { CompanyProductService } from '../../services/company-product.service';
import { MessageService } from '../../services/message.service';
import { ActivatedRoute } from '@angular/router';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-company-product-confirm-dialog',
  templateUrl: 'delete-company-product-confirm-dialog.html',
})
export class DeleteCompanyProductDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteCompanyProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public companyProduct: CompanyProduct
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-company-products',
  templateUrl: './company-products.component.html',
  styleUrls: ['./company-products.component.css']
})
export class CompanyProductsComponent implements OnInit {

  public companyProducts: CompanyProduct[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public productId: number;
  public tableUtil: TableUtil;

   // MatPaginator Inputs
   public length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private companyProductService: CompanyProductService,
    private dialog: MatDialog) {
      this.displayedColumns = ['name', 'options'];
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    this.getCompanyProducts();
  }

  getCompanyProducts(): void {
    this.companyProductService.getCompanyProducts()
      .subscribe(companyProducts => {
        this.companyProducts = companyProducts.slice();
        this.dataSource = new MatTableDataSource(this.companyProducts);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.companyProducts);
      });

  }

  goToCompanyProduct(): void {
    if (this.productId) {
      this.router.navigate(['main/company-product/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/company-product']);
    }
  }

  editCompanyProduct(companyProduct): void {
    if (this.productId) {
      this.router.navigate(['main/company-product/' + companyProduct.id + '/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/company-product/' + companyProduct.id]);
    }
  }

  deleteCompanyProduct(companyProduct: CompanyProduct): void {
    this.openDialog(companyProduct);
  }

  openDialog(companyProduct: CompanyProduct): void {
    const dialogRef = this.dialog.open(DeleteCompanyProductDialogComponent, {
      width: '250px',
      data: companyProduct
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.companyProductService.deleteCompanyProduct(companyProduct.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.companyProducts.indexOf(companyProduct, 0);
              if (index > -1) {
                this.companyProducts.splice(index, 1);
                this.dataSource.data = this.companyProducts;
              }
            }
          });
      }
    });
  }

  returnProduct() {
    if (this.productId) {
      this.router.navigate(['main/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product']);
    }
  }
}
