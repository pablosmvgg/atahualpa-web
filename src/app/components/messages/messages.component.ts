import {Message} from '../../modules/message';
import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../services/message.service';
import {isDevMode, Inject, ViewEncapsulation} from '@angular/core';
import {MatSnackBar, MAT_SNACK_BAR_DATA} from '@angular/material';
import { MessageType } from "../../modules/message-type";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MessagesComponent implements OnInit { 
    
  constructor(public messageService: MessageService, public snackBar: MatSnackBar) {}

  ngOnInit() {
	  this.messageService.getMessage()
	  .subscribe((message:Message) => 
	  	{
	  		if (message.message) {
	  			this.openSnackBar(message);
	  		}
	  	})
  }
  
  openSnackBar(message: Message) {
    
    if (!message) {
      return;
    }

    let classToUse:string = "";
    switch (message.type) {
      case MessageType.Success:
        classToUse = "success-message";
        break;
      case MessageType.Error:
    	classToUse = "error-message";
        break;
      case MessageType.Info:
    	classToUse = "info-message";
        break;
      case MessageType.Warning:
    	classToUse = "warning-message";
        break;
    } 
	 
	this.snackBar.open(message.message, 'Close', {
	   duration: 3000,
	   panelClass: [classToUse]
	});
  }
  
}
//https://material.angular.io/components/snack-bar/examples
//https://stackoverflow.com/questions/47560696/angular-5-and-material-how-to-change-the-background-color-from-snackbar-compon
//https://stackblitz.com/edit/angular-stacb4?file=app%2Fsnack-bar-overview-example.ts
