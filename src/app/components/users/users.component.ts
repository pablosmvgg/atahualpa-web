import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {PageEvent} from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { Message } from '../../modules/message';
import { User } from '../../modules/user';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';
import { MessageType } from '../../modules/message-type';
import { ActivatedRoute } from '@angular/router';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-user-confirm-dialog',
  templateUrl: 'delete-user-confirm-dialog.html',
})
export class DeleteUserDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users: User[];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public commerceId: number;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private dialog: MatDialog,
    private messageService: MessageService
  ) {
    this.displayedColumns = ['name', 'userRoles', 'options'];
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.commerceId = this.route.snapshot.params.id;
    this.userService.getUsers(this.commerceId)
      .subscribe(users => {
        this.users = users.slice();
        this.dataSource = new MatTableDataSource(this.users);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.users);
      });
  }

  goToUser(): void {
    if (this.commerceId) {
      this.router.navigate(['main/user/commerce/' + this.commerceId]);
    }
    else {
      this.router.navigate(['main/user']);
    }
  }

  editUser(user: User): void {
    if (this.commerceId) {
      this.router.navigate(['main/user/' + user.id + '/commerce/' + this.commerceId]);
    }
    else {
      this.router.navigate(['main/user/' + user.id]);
    }
  }

  deleteUser(user: User): void {
    if (this.users.length > 1) {
      this.openDialog(user);
    }
    else {
      const message = new Message('El comercio tiene que tener al menos un usuario.', MessageType.Error);
      this.messageService.add(message);
    }
  }

  openDialog(user: User): void {
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: '250px',
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.userService.deleteUser(user.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.users.indexOf(user, 0);
              if (index > -1) {
                this.users.splice(index, 1);
                this.dataSource.data = this.users;
              }
            }
          });
      }
    });
  }

  returnCommerces() {
    this.router.navigate(['main/commerces']);
  }
}
