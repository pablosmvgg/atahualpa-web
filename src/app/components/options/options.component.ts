import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BranchCommerceService } from '../../services/branch.commerce.service';
import { User } from '../../modules/user';
import { BranchCommerce } from '../../modules/branch-commerce';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {

  public loggedUser: User;
  public branchCommerces: BranchCommerce[];
  public optionsForm: FormGroup;

  constructor(
  private fb: FormBuilder,
	private router: Router,
  private route: ActivatedRoute,
  private userService: UserService,
  private branchCommerceService: BranchCommerceService) {
    this.createForm();
  }

  ngOnInit() {
    this.userService.getLoggedUser().subscribe(
    data => {
      this.loggedUser = data;
      this.branchCommerceService.getBranchCommerces(this.loggedUser.commerce.id)
      .subscribe(dataBC => {
        this.branchCommerces = dataBC;
        this.fillDataForm();
      });
    });
  }

  goToProductStorage() {
	  this.router.navigate(['main/options/product-storage']);
  }

  createForm() {
    this.optionsForm = this.fb.group({
      currentBranchCommerce: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.optionsForm.get('currentBranchCommerce').setValue(this.loggedUser.currentBranchCommerce.id);
  }

  goToExtraDataProductType() {
	  this.router.navigate(['main/options/extra-data-product-type']);
  }

  saveGralOptions(formValues) {

    const currentBranchCommerce = this.branchCommerces.find(branchCommerce => {
      return formValues.currentBranchCommerce === branchCommerce.id;
    });
    console.log(currentBranchCommerce);
    this.loggedUser.currentBranchCommerce = currentBranchCommerce;
    this.userService.saveUser(this.loggedUser, this.loggedUser.commerce.id)
    .subscribe(response => {

      if (response.status === 200 || response.status === 201) {
        // TODO: Mostrar mensaje
      }
    });
  }
  
}
