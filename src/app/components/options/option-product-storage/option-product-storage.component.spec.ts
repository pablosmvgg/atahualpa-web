import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionProductStorageComponent } from './option-product-storage.component';

describe('OptionProductStorageComponent', () => {
  let component: OptionProductStorageComponent;
  let fixture: ComponentFixture<OptionProductStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionProductStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionProductStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
