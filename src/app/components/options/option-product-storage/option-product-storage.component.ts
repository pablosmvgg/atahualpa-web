import { Component, OnInit, Inject } from '@angular/core';
import { TemplateService } from '../../../services/templates/template.service';
import { TemplateTypeService } from '../../../services/templates/template-type.service';
import { TemplateValueTypeService } from '../../../services/templates/template-value-type.service';
import { MessageService } from '../../../services/message.service';
import { TemplateType } from '../../../modules/templates/template-type';
import { TemplateValueType } from '../../../modules/templates/template-value-type';
import { TemplateElement } from '../../../modules/templates/template-element';
import { MessageType } from '../../../modules/message-type';
import { Message } from '../../../modules/message';
import { Template } from '../../../modules/templates/template';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ErrorMessagesUtil } from '../../../utils/error-messages-util';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef } from '@angular/material';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-delete-storage-element-confirm-dialog',
  templateUrl: 'delete-storage-element-confirm-dialog.html',
})
export class DeleteElementStorageDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteElementStorageDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-delete-storage-confirm-dialog',
  templateUrl: 'delete-storage-confirm-dialog.html',
})
export class DeleteStorageDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteStorageDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-option-product-storage',
  templateUrl: './option-product-storage.component.html',
  styleUrls: ['./option-product-storage.component.css']
})
export class OptionProductStorageComponent implements OnInit {

  public templateType: TemplateType;
  public templateValueTypes: TemplateValueType[];
  public template: Template;
  public maxTemplateElementSize = 10;
  public productStorageForm: FormGroup;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private formBuilder: FormBuilder,
    private templateService: TemplateService,
    private templateTypeService: TemplateTypeService,
    private templateValueTypeService: TemplateValueTypeService,
    private messageService: MessageService,
    public errorMessagesUtil: ErrorMessagesUtil) {
      this.template = new Template([]);
      this.fillForm();
  }

  ngOnInit() {
    this.getTemplateTypes();
    this.getTemplateValueTypes();

  }

  fillForm() {
    const elements = [];
    if (this.template.templateElements.length > 0) {
      const templateElements: TemplateElement[]  = this.template.templateElements;
      for (let i = 0; i < templateElements.length; i++) {
        elements.push(this.createValueElement(templateElements[i].id,
            templateElements[i].name,
            templateElements[i].templateValueType.id));
        }
    }

    this.productStorageForm = this.formBuilder.group({
        templateElements: this.formBuilder.array(elements)
    });
  }

  getTemplateTypes(): void {
    this.templateTypeService.getTemplateType(environment.template_type_storage)
      .subscribe(templateType => {
        this.templateType = templateType;
        this.getTemplate();
    });
  }

  getTemplateValueTypes(): void {
    this.templateValueTypeService.getTemplateValueTypes()
      .subscribe(templateValueTypes => {
         this.templateValueTypes = templateValueTypes;
    });
  }

  getTemplate() {
    this.templateService.getTemplateByType(this.templateType.id)
      .subscribe(templates => {
        if (templates && templates.length > 0) {
          this.template = templates[0];
          this.fillForm();
        }
    });
  }

  addTemplateElement() {
    if (this.productStorageForm.value.templateElements.length + 1 <= this.maxTemplateElementSize) {
        const templateElements = this.productStorageForm.get('templateElements') as FormArray;
        templateElements.push(this.createValueElement(null, '', null));
    } else {
        const message = new Message('No se puede agregar más de 10 valores.', MessageType.Error);
        this.messageService.add(message);
    }
  }

  createValueElement(id: number, name: string, templateValueTypeId: number): FormGroup {
    return this.formBuilder.group({
      id: id,
      name: [name, Validators.required],
      templateValueTypeId: [templateValueTypeId, Validators.required]
    });
  }

  deleteTemplateElement(index) {
    const control = <FormArray>this.productStorageForm.controls.templateElements;
    if (control.controls[index].value.id) {
      this.openDialog(control, index);
    }
    else {
      control.removeAt(index);
    }
  }

  openDialog(element: FormArray, index: number): void {
    const dialogRef = this.dialog.open(DeleteElementStorageDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        element.removeAt(index);
      }
    });
  }

  openDialogCompleteDelete(): void {
    const dialogRef = this.dialog.open(DeleteStorageDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.templateService.deleteTemplate(this.template.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              this.router.navigate(['main/options']);
            }
        });
      }
    });
  }

  clearProductStorage() {
    if (this.template.id) {
      this.openDialogCompleteDelete();
    }

  }

  onSubmitProductStorageOption() {

    const elementsToSave = this.productStorageForm.value.templateElements;
    const templateElements: TemplateElement[] = [];
    elementsToSave.forEach(elementToSave => {
      const templateValueType: TemplateValueType = this.templateValueTypes.find(templateValueTypeFound => {
        return templateValueTypeFound.id === elementToSave.templateValueTypeId;
      });
      const templateElement: TemplateElement = new TemplateElement(elementToSave.id, elementToSave.name, templateValueType);
      templateElements.push(templateElement);
    });

    if (!this.template.id) {
      this.template.name = this.templateType.name;
      this.template.templateType = this.templateType;
    }
    this.template.templateElements = templateElements;
    this.templateService.saveTemplate(this.template)
    .subscribe(response => {

      if (response.status === 200 || response.status === 201) {
        this.router.navigate(['main/options']);
      }
    });
  }

  validateSaveButtonVisibility() {
    return !this.productStorageForm.valid || this.productStorageForm.value.templateElements.length === 0;
  }

  getFormTemplateElements() {
      return <FormArray>this.productStorageForm.get('templateElements');
  }

}
