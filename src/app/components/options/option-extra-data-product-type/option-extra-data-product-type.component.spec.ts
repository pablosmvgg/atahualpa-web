import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionExtraDataProductTypeComponent } from './option-extra-data-product-type.component';

describe('OptionExtraDataProductTypeComponent', () => {
  let component: OptionExtraDataProductTypeComponent;
  let fixture: ComponentFixture<OptionExtraDataProductTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionExtraDataProductTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionExtraDataProductTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
