import { Component, OnInit } from '@angular/core';
import { ProductTypeService } from '../../../services/product-type.service';
import { ProductType } from '../../../modules/product-type';
import { TemplateTypeService } from '../../../services/templates/template-type.service';
import { TemplateType } from '../../../modules/templates/template-type';
import { TemplateService } from '../../../services/templates/template.service';
import { Template } from '../../../modules/templates/template';
import { TemplateElement } from '../../../modules/templates/template-element';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ErrorMessagesUtil } from '../../../utils/error-messages-util';
import { TemplateValueType } from '../../../modules/templates/template-value-type';
import { TemplateValueTypeService } from '../../../services/templates/template-value-type.service';
import { Message } from '../../../modules/message';
import { MessageType } from '../../../modules/message-type';
import { MessageService } from '../../../services/message.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-delete-extra-data-element-confirm-dialog',
  templateUrl: 'delete-extra-data-element-confirm-dialog.html',
})
export class DeleteElementExtraDataDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteElementExtraDataDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-delete-extra-data-confirm-dialog',
  templateUrl: 'delete-extra-data-confirm-dialog.html',
})
export class DeleteExtraDataDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteExtraDataDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-option-extra-data-product-type',
  templateUrl: './option-extra-data-product-type.component.html',
  styleUrls: ['./option-extra-data-product-type.component.css']
})
export class OptionExtraDataProductTypeComponent implements OnInit {

  public productTypes: ProductType[];
  public templateType: TemplateType;
  public templateValueTypes: TemplateValueType[];
  public templates: Template[];
  public maxTemplateElementSize = 10;
  public productExtraDataForm: FormGroup;

  constructor(private dialog: MatDialog,
    private router: Router,
    private formBuilder: FormBuilder,
    private productTypeService: ProductTypeService,
    private templateTypeService: TemplateTypeService,
    private templateService: TemplateService,
    private templateValueTypeService: TemplateValueTypeService,
    private messageService: MessageService,
    public errorMessagesUtil: ErrorMessagesUtil) {
      this.templates = [];
      this.fillForm();
    }

  ngOnInit() {
    this.getTemplateValueTypes();
    this.getTemplateType();
  }

  getTemplateType(): void {
    this.templateTypeService.getTemplateType(environment.template_type_extra_data)
      .subscribe(templateType => {
        this.templateType = templateType;
        this.getProductTypes();
    });
  }

   getProductTypes() {
    this.productTypeService.getProductTypes()
    .subscribe(response => {
        this.productTypes = response;
        this.getTemplate();
    });
  }


  getTemplate() {
    this.templateService.getTemplateByType(this.templateType.id)
      .subscribe(templates => {
        if (templates && templates.length > 0) {
          this.templates = templates;
        }
        this.fillForm();
    });
  }

  getTemplateValueTypes(): void {
    this.templateValueTypeService.getTemplateValueTypes()
      .subscribe(templateValueTypes => {
         this.templateValueTypes = templateValueTypes;
    });
  }

  fillForm() {
    const elements = [];
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        const productType = this.productTypes[i];
        const elementsByType = [];
        let templateId: number;
        if (this.templates.length > 0) {
            templateId = this.templates[i].id;
            const templateElements: TemplateElement[]  = this.templates[i].templateElements;
            for (let j = 0; j < templateElements.length; j++) {
            elementsByType.push(this.createValueElement(templateElements[j].id,
                templateElements[j].name,
                templateElements[j].templateValueType.id));
            }
        }
        const groupElement = this.formBuilder.group({
            templateId: templateId,
            productTypeId: [productType.id, Validators.required],
            productTypeName: [productType.name, Validators.required],
            productTypeFields: this.formBuilder.array(elementsByType)
        });
        elements.push(groupElement);
      }

    }
    this.productExtraDataForm = this.formBuilder.group({
        productTypes: this.formBuilder.array(elements)
    });
  }

  createValueElement(id: number, name: string, templateValueTypeId: number): FormGroup {
    return this.formBuilder.group({
      id: id,
      name: [name, Validators.required],
      templateValueTypeId: [templateValueTypeId, [Validators.required]]
    });
  }

  clearProductExtraData() {
    if (this.templates[0].id) {
      this.openDialogCompleteDelete();
    }

  }

  openDialog(element: FormArray, index: number): void {
    const dialogRef = this.dialog.open(DeleteElementExtraDataDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        element.removeAt(index);
      }
    });
  }

  openDialogCompleteDelete(): void {
    const dialogRef = this.dialog.open(DeleteExtraDataDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.templateService.deleteTemplatesByType(this.templateType.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              this.router.navigate(['main/options']);
            }
        });
      }
    });
  }

  getFormProductTypes() {
    return <FormArray>this.productExtraDataForm.get('productTypes');
  }

  getProductTypeFields(productType) {
      return <FormArray>productType.get('productTypeFields');
  }

  addTemplateElement(productTypeFields) {

    if (productTypeFields.length + 1 <= this.maxTemplateElementSize) {
        productTypeFields.push(this.createValueElement(null, '', null));
    } else {
        const message = new Message('No se puede agregar más de 10 valores.', MessageType.Error);
        this.messageService.add(message);
    }
  }

  deleteTemplateElement(productType, index) {
    const control = <FormArray>productType.controls.productTypeFields;
    if (control.controls[index].value.id) {
      this.openDialog(control, index);
    }
    else {
      control.removeAt(index);
    }
  }

  validateSaveButtonVisibility() {

    return !this.productExtraDataForm.valid;
  }

  generateNewTemplates() {
    const elementsProductTypes = this.productExtraDataForm.value.productTypes;
    const templatesToSave = [];
    elementsProductTypes.forEach(elementProductType => {
      const productFields: TemplateElement[] = [];
      elementProductType.productTypeFields.forEach(element => {
        const templateValueType: TemplateValueType = this.templateValueTypes.find(templateValueTypeFound => {
            return templateValueTypeFound.id === element.templateValueTypeId;
        });
        const templateElement: TemplateElement = new TemplateElement(element.id, element.name, templateValueType);
        productFields.push(templateElement);
      });
      const template = new Template(productFields);

      template.id = elementProductType.templateId;
      template.name = elementProductType.productTypeName;
      template.templateType = this.templateType;
      templatesToSave.push(template);
    });

    return templatesToSave;
  }

  onSubmitProductExtraData() {

    const templatesToSave = this.generateNewTemplates();

    this.templateService.saveTemplates(templatesToSave)
    .subscribe(response => {
      if (response && response.status === 200 || response.status === 201) {
        this.router.navigate(['main/options']);
      }
   });

  }

}
