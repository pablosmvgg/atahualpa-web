import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {CommerceService} from '../../services/commerce.service';
import {Commerce} from '../../modules/commerce';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-admin-commerce',
  templateUrl: './admin-commerce.component.html',
  styleUrls: ['./admin-commerce.component.css']
})
export class AdminCommerceComponent implements OnInit {

  public commerce: Commerce;
  public commerceForm: FormGroup;
  public commerceValidationMessages;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private commerceService: CommerceService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.commerceValidationMessages = {
      'required': ' es requerido',
      'email': ' no es un email válido'
    };

    this.createForm();
  }

  ngOnInit() {
    if (this.route.snapshot.params.id) {
      this.commerceService.getCommerceById(this.route.snapshot.params.id)
        .subscribe(response => {
            this.commerce = response;
            this.fillDataForm();
        });
    }
    else {
      this.commerce = new Commerce();
    }
  }

  getErrorMessage(element, name) {
    if (element.hasError('required')) {
      return name + this.commerceValidationMessages.required;
    }
  }

  createForm() {
    this.commerceForm = this.fb.group({
      name: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.commerceForm.get('name').setValue(this.commerce.name);
  }

  onSubmitSupplier(commerceForm) {
    this.commerce.name = commerceForm.name;
    this.commerceService.saveCommerce(this.commerce)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.router.navigate(['main/commerces']);
        }
      });
  }
  goBackCommerce() {
    this.router.navigate(['main/commerces']);
  }
}
