import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCommerceComponent } from './admin-commerce.component';

describe('AdminCommerceComponent', () => {
  let component: AdminCommerceComponent;
  let fixture: ComponentFixture<AdminCommerceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCommerceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
