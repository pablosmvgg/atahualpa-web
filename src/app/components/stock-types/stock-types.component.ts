import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatTableDataSource, PageEvent, MatPaginator, MatDialog, Sort } from '@angular/material';
import { ProductStockType } from '../../modules/product-stock-type';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductStockTypeService } from '../../services/product-stock-type.service';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-stock-type-confirm-dialog',
  templateUrl: 'delete-stock-type-confirm-dialog.html',
})
export class DeleteStockTypeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteStockTypeDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-stock-types',
  templateUrl: './stock-types.component.html',
  styleUrls: ['./stock-types.component.css']
})
export class StockTypesComponent implements OnInit {

  public stockTypes: ProductStockType[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public productId: number;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private productStockTypeService: ProductStockTypeService,
    private dialog: MatDialog) {
      this.displayedColumns = ['name', 'options'];
    }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    this.getStockTypes();
  }

  getStockTypes(): void {
    this.productStockTypeService.getProductStockTypes()
      .subscribe(stockTypes => {
        this.stockTypes = stockTypes.slice();
        this.dataSource = new MatTableDataSource(this.stockTypes);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.stockTypes);
      });
  }

  goToStockType(): void {
    if (this.productId) {
      this.router.navigate(['main/stock-type/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/stock-type']);
    }
  }

  returnStock() {
    // TODO: sumar el guardado temporal de los datos para volver al stock con lo llenado
    this.router.navigate(['main/product/' + this.productId + '/stock/' ]);
  }

  editStockType(stockType): void {
    this.router.navigate(['main/stock-type/' + stockType.id + '/product/' + this.productId]);
  }

  deleteStockType(stockType: ProductStockType): void {
    this.openDialog(stockType);
  }

  openDialog(stockType: ProductStockType): void {
    const dialogRef = this.dialog.open(DeleteStockTypeDialogComponent, {
      width: '250px',
      data: stockType
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.productStockTypeService.deleteProductStockType(stockType.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.stockTypes.indexOf(stockType, 0);
              if (index > -1) {
                this.stockTypes.splice(index, 1);
                this.dataSource.data = this.stockTypes;
              }
            }
          });
      }
    });
  }

  returnProductStock() {
    this.router.navigate(['main/product/' + this.productId + '/stock']);
  }

}
