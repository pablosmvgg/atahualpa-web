import { Product } from '../../modules/product';
import { UnitType } from '../../modules/unit-type';
import { CompanyProduct } from '../../modules/company-product';
import { ProductService } from '../../services/product.service';
import { CompanyProductService } from '../../services/company-product.service';
import { UnitTypeService } from '../../services/unit-type.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';
import { ProductTypeService } from '../../services/product-type.service';
import { ProductType } from '../../modules/product-type';
import { Template } from '../../modules/templates/template';
import { TemplateService } from '../../services/templates/template.service';
import { TemplateValue } from '../../modules/templates/template-value';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public product: Product;
  public productForm: FormGroup;
  public unitTypes: UnitType[];
  public companyProducts: CompanyProduct[];
  public productTypes: ProductType[];
  public templateTypeId = 2;
  public templatesExtraData: Template[];
  public finalPricePorc: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private companyProductService: CompanyProductService,
    private unitTypeService: UnitTypeService,
    private productTypeService: ProductTypeService,
    private templateService: TemplateService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.createForm();
    this.productForm.get('percentageProfit').valueChanges.subscribe(() => {
      this.calculateFinalPrice();
    });
    this.productForm.get('price').valueChanges.subscribe(() => {
      this.calculateFinalPrice();
    });
  }

  ngOnInit() {

    this.getUnitTypes();
    this.getCompanyProducts();
    this.getProductTypes();
    this.finalPricePorc = true;
  }

  getExtraDataTemplate() {

    this.templateService.getTemplateByType(this.templateTypeId)
      .subscribe(templates => {
        if (templates && templates.length > 0) {
          this.templatesExtraData = templates;

          const retrievedProduct = localStorage.getItem('productTemp');
          if (retrievedProduct) {
            this.product = JSON.parse(retrievedProduct);
            localStorage.removeItem('productTemp');
            this.fillDataForm();
          }
          else {
            if (this.route.snapshot.params.id) {
              this.productService.getProduct(this.route.snapshot.params.id)
              .subscribe(response => {
                this.product = response;
                this.fillDataForm();
              });
            }
         }
      }
      else {
        this.product = new Product();
      }
    });
  }

  recoverProductTemp() {
    const retrievedProduct = localStorage.getItem('productTemp');
    if (retrievedProduct) {
      this.product = JSON.parse(retrievedProduct);
      localStorage.removeItem('productTemp');
    }
  }

  getUnitTypes() {
    this.unitTypeService.getUnitTypes()
      .subscribe(response => {
        this.unitTypes = response;
    });
  }

  getCompanyProducts() {
    this.companyProductService.getCompanyProducts()
      .subscribe(response => {
        this.companyProducts = response;
    });
  }

  getProductTypes() {
    this.productTypeService.getProductTypes()
      .subscribe(response => {
        this.productTypes = response;
        this.getExtraDataTemplate();
    });
  }

  createForm() {
    this.productForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      productType: [null, Validators.required],
      price: ['', Validators.required],
      percentageProfit: [{value: '', disabled: false}],
      finalPrice: [{value: '', disabled: true}, Validators.required],
      unitType: [null, Validators.required],
      unitAmount: ['', Validators.required],
      barCode: ['', Validators.required],
      companyProduct: [null, Validators.required],
      minQuantity: ['', Validators.required],
      extraFields: this.formBuilder.array([]),
    });
  }

  fillDataForm() {
    this.productForm.get('name').setValue(this.product.name);
    this.productForm.get('description').setValue(this.product.description);
    this.productForm.get('productType').setValue(this.product.productType ? this.product.productType.id : 0);
    this.productForm.get('price').setValue(this.product.price);
    this.productForm.get('percentageProfit').setValue(this.product.percentageProfit);
    this.productForm.get('finalPrice').setValue(this.product.finalPrice);
    this.productForm.get('unitType').setValue(this.product.unitType ? this.product.unitType.id : 0);
    this.productForm.get('unitAmount').setValue(this.product.unitAmount);
    this.productForm.get('barCode').setValue(this.product.barCode);
    this.productForm.get('companyProduct').setValue(this.product.companyProduct ? this.product.companyProduct.id : 0);
    this.productForm.get('minQuantity').setValue(this.product.minQuantity);

    this.changeProductType(true);
  }

  onSubmitProduct() {

    this.fillProduct();
    this.productService.saveProduct(this.product)
      .subscribe(response => {
        if (response && response.status === 200 || response.status === 201) {
          this.router.navigate(['main/products']);
        }
     });
  }

  fillProduct() {
    this.product.name = this.productForm.value.name;
    this.product.description = this.productForm.value.description;
    const productType = this.productTypes.find(productTypeFound => {
      return productTypeFound.id === this.productForm.value.productType;
    });
    this.product.productType = productType;
    this.product.price = this.productForm.value.price;
    this.product.percentageProfit = this.productForm.value.percentageProfit;
    this.product.finalPrice = this.productForm.value.finalPrice;
    const unitType = this.unitTypes.find(unitTypeFound => {
      return unitTypeFound.id === this.productForm.value.unitType;
    });
    this.product.unitType = unitType;
    this.product.unitAmount = this.productForm.value.unitAmount;
    this.product.barCode = this.productForm.value.barCode;
    const companyProduct = this.companyProducts.find(companyProductFound => {
      return companyProductFound.id === this.productForm.value.companyProduct;
    });
    this.product.companyProduct = companyProduct;
    this.product.minQuantity = this.productForm.value.minQuantity;
    this.product.extraFields = [];
    if (this.productForm.get('extraFields')) {
      this.productForm.get('extraFields').value.forEach(extraField => {
        const template = this.templatesExtraData.find(templateFound => {
          return templateFound.name === this.product.productType.name;
        });
        const templateElementToSave = template.templateElements.find(templateElement => {
            return templateElement.id === extraField.elementId;
        });
        let elementValue = null;
        if (Array.isArray(extraField.elementValue)) {
          elementValue = new TemplateValue(extraField.elementValue, templateElementToSave);
        }
        else {
          elementValue = new TemplateValue([extraField.elementValue], templateElementToSave);
        }
        this.product.extraFields.push(elementValue);
      });
    }
  }

  cancelProduct() {
    this.router.navigate(['main/products']);
  }

  saveProductTempLocal() {
    this.fillProduct();
    localStorage.setItem('productTemp', JSON.stringify(this.product));
  }

  goToCompanyProduct() {

    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/company-product/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/company-product']);
    }
  }

  goToCompanyProducts() {
    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/company-products/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/company-products']);
    }
  }

  goToUnitType() {
    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/unit-type/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/unit-type']);
    }
  }

  goToUnitTypes() {
    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/unit-types/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/unit-types']);
    }
  }

  goToProductType() {
    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/product-type/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/product-type']);
    }
  }

  goToProductTypes() {
    this.saveProductTempLocal();
    if (this.product.id) {
      this.router.navigate(['main/product-types/product/' + this.product.id]);
    }
    else {
      this.router.navigate(['main/product-types']);
    }
  }

  showExtraData(templateExtraData: Template) {

    const productType = this.productTypes.find(productTypeFound => {
      return productTypeFound.id === this.productForm.value.productType;
    });
    if (productType && productType.name === templateExtraData.name) {
        return false;
    }
    return true;

  }

  changeProductType(firstTime: boolean) {
    // TODO: agregar un mensaje de warning al cambiar el tipo si tiene valores guardados
    if (this.productTypes) {
      const productTypeId = this.productForm.get('productType').value;
      const productType = this.productTypes.find(productTypeFound => {
        return productTypeFound.id === productTypeId;
      });
      const elements = [];
      if (this.templatesExtraData) {

          this.templatesExtraData.forEach(templateExtraData => {
          if (templateExtraData.name === productType.name) {
            templateExtraData.templateElements.forEach(templateElement => {

                let valueToShow = [];
                if (firstTime && this.product.extraFields) {
                  const elementValue = this.product.extraFields.find(elementValueFound => {
                      return elementValueFound.templateElement.name === templateElement.name;
                    });
                  valueToShow = elementValue.valuez;
                }

                const element = this.formBuilder.group({
                    elementId: [templateElement.id, Validators.required],
                    elementName: [templateElement.name, Validators.required],
                    elementType: [templateElement.templateValueType.id, Validators.required],
                    elementValue: [valueToShow]
                });
                elements.push(element);
            });
          }
        });
      }
    this.productForm.controls.extraFields  = this.formBuilder.array(elements);
    }

  }

  getFormExtraData() {
    return <FormArray> this.productForm.get('extraFields');
  }

  changeFinalPriceStatus() {
    this.finalPricePorc = !this.finalPricePorc;
    if (this.finalPricePorc) {
        this.productForm.get('percentageProfit').enable();
        this.productForm.get('finalPrice').disable();
    }
    else {
        this.productForm.get('percentageProfit').disable();
        this.productForm.get('finalPrice').enable();
    }
  }

  calculateFinalPrice() {
      if (this.productForm.controls['percentageProfit'].value
        && this.productForm.controls['price'].value
        && this.productForm.controls['percentageProfit'].enabled) {
        if (this.productForm.get('price').value
            && !isNaN(this.productForm.get('price').value)
            && !isNaN(this.productForm.get('price').value)) {

            // tslint:disable-next-line:radix
            const finalPrice = parseFloat(this.productForm.get('price').value) +
                (parseFloat(this.productForm.get('price').value) * parseFloat(this.productForm.get('percentageProfit').value)) / 100;
            this.productForm.get('finalPrice').setValue(finalPrice);
        }
     }
  }

}
