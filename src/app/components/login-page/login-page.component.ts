
import {throwError as observableThrowError } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder,
  ) {
      this.createForm();
  }

  ngOnInit() {
    if (sessionStorage.getItem('userToken') !== null) {
      this.router.navigate(['main/dashboard']);
    }
  }

  createForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(): void {
    const userFound = this.userService
        .loginUser(this.loginForm.get('username').value, this.loginForm.get('password').value)
        .subscribe(
          data => {
            if (data) {
              sessionStorage.setItem('userToken', data.sessionId);
              this.router.navigate(['main/dashboard']);
            }
            return true;
          },
          error => {
            console.error('Error!' + error);
            return observableThrowError(error);
          });
  }

}
