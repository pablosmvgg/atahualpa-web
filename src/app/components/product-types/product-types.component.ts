import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatTableDataSource, PageEvent, MatPaginator, MatDialog } from '@angular/material';
import { ProductType } from '../../modules/product-type';
import { TableUtil } from '../../utils/table-util';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductTypeService } from '../../services/product-type.service';

@Component({
  selector: 'app-delete-product-type-confirm-dialog',
  templateUrl: 'delete-product-type-confirm-dialog.html',
})
export class DeleteProductTypeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteProductTypeDialogComponent>
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-product-types',
  templateUrl: './product-types.component.html',
  styleUrls: ['./product-types.component.css']
})
export class ProductTypesComponent implements OnInit {

  public productTypes: ProductType[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public productId: number;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private productTypeService: ProductTypeService,
    private dialog: MatDialog) {
      this.displayedColumns = ['name', 'options'];
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    this.getProductTypes();
  }

  getProductTypes(): void {
    this.productTypeService.getProductTypes()
      .subscribe(productTypes => {
        this.productTypes = productTypes.slice();
        this.dataSource = new MatTableDataSource(this.productTypes);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.productTypes);
      });
  }

  goToProductType(): void {
    if (this.productId) {
      this.router.navigate(['main/product-type/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product-type']);
    }
  }

  editProductType(productType): void {
    if (this.productId) {
      this.router.navigate(['main/product-type/' + productType.id + '/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product-type/' + productType.id]);
    }

  }

  deleteProductType(productType: ProductType): void {
    this.openDialog(productType);
  }

  openDialog(unitType: ProductType): void {
    const dialogRef = this.dialog.open(DeleteProductTypeDialogComponent, {
      width: '250px',
      data: unitType
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.productTypeService.deleteProductType(unitType.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.productTypes.indexOf(unitType, 0);
              if (index > -1) {
                this.productTypes.splice(index, 1);
                this.dataSource.data = this.productTypes;
              }
            }
          });
      }
    });
  }

  returnProduct() {
    if (this.productId) {
      this.router.navigate(['main/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product']);
    }
  }
}
