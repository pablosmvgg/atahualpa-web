import { CompanyProduct } from '../../modules/company-product';
import { CompanyProductService } from '../../services/company-product.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-company-product',
  templateUrl: './company-product.component.html',
  styleUrls: ['./company-product.component.css']
})
export class CompanyProductComponent implements OnInit {

  public companyProduct: CompanyProduct;
  public companyProductForm: FormGroup;
  public productId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private companyProductService: CompanyProductService,
    public errorMessagesUtil: ErrorMessagesUtil) {

    this.createForm();
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    if (this.route.snapshot.params.id) {
      this.companyProductService.getCompanyProduct(this.route.snapshot.params.id)
        .subscribe(response => {
          this.companyProduct = response;
          this.fillDataForm();
        });
    }
    else {
      this.companyProduct = new CompanyProduct();
    }
  }

  createForm() {
    this.companyProductForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],

    });
  }

  fillDataForm() {
    this.companyProductForm.get('name').setValue(this.companyProduct.name);
    this.companyProductForm.get('description').setValue(this.companyProduct.description);
  }

  onSubmitCompanyProduct() {
    this.companyProduct.name = this.companyProductForm.value.name;
    this.companyProduct.description = this.companyProductForm.value.description;

    this.companyProductService.saveCompanyProduct(this.companyProduct)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.returnToCompanyProducts();
        }
     });
  }

  cancelCompanyProduct() {
    this.returnToCompanyProducts();
  }

  returnToCompanyProducts() {
    if (this.productId) {
      this.router.navigate(['main/company-products/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/company-products']);
    }
  }

}
