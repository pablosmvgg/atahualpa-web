
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.getLoggedUser().subscribe(
    data => {
      // TODO: completar esta parte para sacar el REST o llenar la pantalla con estos datos
    },
    error => {
      this.doLogoutAction();
    });
  }

  goToSuppliers() {
    this.router.navigate(['main/suppliers']);
  }

  logout() {
    this.userService.logout().subscribe(
      data => {
         this.doLogoutAction();
      },
      error => {
       console.error('Error!' + error);
       return observableThrowError(error);
     });
  }

  doLogoutAction() {
    sessionStorage.removeItem('userToken');
    this.router.navigate(['login']);
  }

}
