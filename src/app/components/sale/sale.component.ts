import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { SaleItem } from '../../modules/saleItem';
import { ProductStock } from '../../modules/product-stock';
import { ProductStockService } from '../../services/product-stock.service';
import { ProductSearch } from '../../modules/product-search';

@Component({
	selector: 'app-sale',
	templateUrl: './sale.component.html',
	styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {

	public saleForm: FormGroup;
	public dataSource: MatTableDataSource<any>;
	public displayedColumns: string[];
	public saleItems: SaleItem[];
	public productsFound: ProductSearch[];

	constructor(private formBuilder: FormBuilder,
		public productStockService: ProductStockService) {
		this.createForm();
		this.displayedColumns = ['name', 'options'];
		this.saleItems = [];
		this.dataSource = new MatTableDataSource(this.saleItems);
		this.productsFound = [];
	}

	ngOnInit() {

	}

	createForm() {
		this.saleForm = this.formBuilder.group({
			productFilter: ['', []]
		});
	}

	searchProduct() {
		const productSearchData = this.saleForm.get('productFilter').value;
		if (productSearchData.length >= 3) {

			this.productStockService.searchProducts(productSearchData)
				.subscribe(productSearchs => {
					this.productsFound = productSearchs;
					console.log(this.productsFound);
				});

			/*const saleItem = new SaleItem();
			saleItem.productStock = new ProductStock();
			saleItem.productStock.product = new Product();
			saleItem.productStock.product.name = productSearch;

			this.saleItems.push(saleItem);
			this.dataSource.data = this.saleItems;*/
		}
		else {
			this.productsFound = [];
		}
	}

	removeSaleItem(saleItem: SaleItem) {
		const index = this.saleItems.indexOf(saleItem, 0);
		if (index > -1) {
			this.saleItems.splice(index, 1);
			this.dataSource.data = this.saleItems;
		}
	}

	onSubmitSale() {
	}

	selectProduct(productSelected: ProductStock) {
		console.log(productSelected);

		const productSearched = this.dataSource.data.find(currSaleItem => {
			return currSaleItem.productStock.id === productSelected.id;
		});
		if (!productSearched) {
			this.productsFound = [];
			this.saleForm.get('productFilter').setValue('');

			const saleItem = new SaleItem();
			saleItem.productStock = productSelected;
			this.saleItems.push(saleItem);
			this.dataSource.data = this.saleItems;
		}
	}

}
