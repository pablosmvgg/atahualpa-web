import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {Router} from '@angular/router';
import { Commerce } from '../../modules/commerce';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatPaginator} from '@angular/material';
import { CommerceService } from '../../services/commerce.service';
import { TableUtil } from '../../utils/table-util';

@Component({
  selector: 'app-delete-commerce-confirm-dialog',
  templateUrl: 'delete-commerce-confirm-dialog.html',
})
export class DeleteCommerceDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteCommerceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public commerce: Commerce
  ) {}

  onYesClick(): void {
    this.dialogRef.close({buttonYesPressed: true});
  }

  onNoClick(): void {
    this.dialogRef.close({buttonYesPressed: false});
  }
}

@Component({
  selector: 'app-commerces',
  templateUrl: './commerces.component.html',
  styleUrls: ['./commerces.component.css']
})
export class CommercesComponent implements OnInit {

  public commerces: Commerce[] = [];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;
  public tableUtil: TableUtil;

  // MatPaginator Inputs
  length = 0;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router,
    private commerceService: CommerceService,
    private dialog: MatDialog) {
      this.displayedColumns = ['name', 'options'];
  }

  ngOnInit() {
    this.getCommerces();
  }

  getCommerces(): void {
    this.commerceService.getCommerces()
      .subscribe(commerces => {
        this.commerces = commerces.slice();
        this.dataSource = new MatTableDataSource(this.commerces);
        this.length = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        this.tableUtil = new TableUtil(this.dataSource, this.commerces);
      });

  }

  goToCommerce(): void {
    this.router.navigate(['main/admin-commerce']);
  }

  editCommerce(commerce: Commerce): void {
    this.router.navigate(['main/admin-commerce/' + commerce.id]);
  }

  deleteCommerce(commerce: Commerce): void {
    this.openDialog(commerce);
  }

  openDialog(commerce: Commerce): void {
    const dialogRef = this.dialog.open(DeleteCommerceDialogComponent, {
      width: '250px',
      data: commerce
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.buttonYesPressed) {
        this.commerceService.deleteCommerce(commerce.id)
          .subscribe(response => {
            if (response.status === 200 || response.status === 201) {
              const index = this.commerces.indexOf(commerce, 0);
              if (index > -1) {
                this.commerces.splice(index, 1);
                this.dataSource.data = this.commerces;
              }
            }
          });
      }
    });
  }

  goToCommerceBranches(commerce: Commerce) {
    this.router.navigate(['main/branches/commerce/' + commerce.id]);
  }

  goToUsers(commerce: Commerce) {
    this.router.navigate(['main/users/commerce/' + commerce.id]);
  }
}
