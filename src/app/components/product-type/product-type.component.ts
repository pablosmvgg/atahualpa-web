import { Component, OnInit } from '@angular/core';
import { ProductType } from '../../modules/product-type';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductTypeService } from '../../services/product-type.service';
import { ErrorMessagesUtil } from '../../utils/error-messages-util';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.css']
})
export class ProductTypeComponent implements OnInit {

  public productType: ProductType;
  public productTypeForm: FormGroup;
  public productId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productTypeService: ProductTypeService,
    public errorMessagesUtil: ErrorMessagesUtil
  ) {

    this.createForm();
  }

  ngOnInit() {
    this.productId = this.route.snapshot.params.productId;
    if (this.route.snapshot.params.id) {
      this.productTypeService.getProductType(this.route.snapshot.params.id)
        .subscribe(response => {
          this.productType = response;
          this.fillDataForm();
        });
    }
    else {
      this.productType = new ProductType();
    }
  }

  createForm() {
    this.productTypeForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  fillDataForm() {
    this.productTypeForm.get('name').setValue(this.productType.name);
    this.productTypeForm.get('description').setValue(this.productType.description);
  }

  onSubmitProductType() {
    this.productType.name = this.productTypeForm.value.name;
    this.productType.description = this.productTypeForm.value.description;

    this.productTypeService.saveProductType(this.productType)
      .subscribe(response => {
        if (response.status === 200 || response.status === 201) {
          this.returnToProductTypes();
        }
     });
  }

  cancelProductType() {
    this.returnToProductTypes();
  }

  returnToProductTypes() {
    if (this.productId) {
      this.router.navigate(['main/product-types/product/' + this.productId]);
    }
    else {
      this.router.navigate(['main/product-types']);
    }
  }

}
