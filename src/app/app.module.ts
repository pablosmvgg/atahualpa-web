import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './/app-routing.module';
import { AuthGuard } from './auth.guard';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ModalModule} from 'ngx-bootstrap/modal';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OverlayContainer} from '@angular/cdk/overlay';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatSelectModule,
    MatSnackBarModule,
    MatExpansionModule  } from '@angular/material';
  import { TagInputModule } from 'ngx-chips';

// Components
import { MessagesComponent } from './components/messages/messages.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { CommercesComponent, DeleteCommerceDialogComponent } from './components/commerces/commerces.component';
import { CommerceComponent } from './components/commerce/commerce.component';
import { SuppliersComponent, DeleteSupplierDialogComponent } from './components/suppliers/suppliers.component';
import { MenuComponent } from './components/menu-bar/menu/menu.component';
import { SupplierComponent } from './components/supplier/supplier.component';
import { BranchesComponent, DeleteBranchDialogComponent } from './components/branches/branches.component';
import { BranchComponent } from './components/branch/branch.component';
import { UsersComponent, DeleteUserDialogComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import { LoggedComponent } from './components/logged/logged.component';
import { ProductsComponent, DeleteProductDialogComponent } from './components/products/products.component';
import { ProductComponent } from './components/product/product.component';
import { CompanyProductsComponent, DeleteCompanyProductDialogComponent } from './components/company-products/company-products.component';
import { CompanyProductComponent } from './components/company-product/company-product.component';
import { UnitTypesComponent, DeleteUnitTypeDialogComponent } from './components/unit-types/unit-types.component';
import { UnitTypeComponent } from './components/unit-type/unit-type.component';
import { OptionsComponent } from './components/options/options.component';
import { OptionProductStorageComponent, DeleteElementStorageDialogComponent, DeleteStorageDialogComponent } from './components/options/option-product-storage/option-product-storage.component';
import { OptionExtraDataProductTypeComponent, DeleteElementExtraDataDialogComponent, DeleteExtraDataDialogComponent, } from './components/options/option-extra-data-product-type/option-extra-data-product-type.component';
import { StockComponent } from './components/stock/stock.component';
import { ProductTypesComponent, DeleteProductTypeDialogComponent } from './components/product-types/product-types.component';
import { ProductTypeComponent } from './components/product-type/product-type.component';
import { AdminCommerceComponent } from './components/admin-commerce/admin-commerce.component';
import { StockTypesComponent, DeleteStockTypeDialogComponent } from './components/stock-types/stock-types.component';
import { StockTypeComponent } from './components/stock-type/stock-type.component';
import { SaleComponent } from './components/sale/sale.component';

// Services
import {MessageService} from './services/message.service';
import {CustomHTTPInterceptor} from './services/config/custom.http.interceptor';
import {UserService} from './services/user.service';
import {SupplierService} from './services/supplier.service';
import {CommerceService} from './services/commerce.service';
import {BranchCommerceService} from './services/branch.commerce.service';
import {UserRoleService} from './services/user-role.service';
import {ProductService} from './services/product.service';
import {CompanyProductService} from './services/company-product.service';
import {UnitTypeService} from './services/unit-type.service';
import {TemplateService} from './services/templates/template.service';
import {TemplateTypeService} from './services/templates/template-type.service';
import {TemplateValueTypeService} from './services/templates/template-value-type.service';
import { ProductStockService } from './services/product-stock.service';
import { ProductTypeService } from './services/product-type.service';


// Utils
import { ErrorMessagesUtil } from './utils/error-messages-util';
import { RoleUtil } from './utils/role-util';



// https://angular.io/tutorial
// ng generate component dashboard
// ng generate service message --module=app

// Internationalization
// https://angular.io/guide/i18n
// Multilanguage: https://medium.com/@feloy/deploying-an-i18n-angular-app-with-angular-cli-fc788f17e358


@NgModule({
   exports: [
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    MatToolbarModule,
    MatSelectModule,
    MatSnackBarModule,
    MatExpansionModule
  ],
   declarations: []
})
export class MaterialModule {}


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    DashboardComponent,
    LoginPageComponent,
    LoggedComponent,
    SuppliersComponent,
    MenuBarComponent,
    MenuComponent,
    SupplierComponent,
    DeleteSupplierDialogComponent,
    CommerceComponent,
    BranchesComponent,
    DeleteBranchDialogComponent,
    BranchComponent,
    UsersComponent,
    UserComponent,
    DeleteUserDialogComponent,
    ProductsComponent,
    DeleteProductDialogComponent,
    ProductComponent,
    CompanyProductsComponent,
    CompanyProductComponent,
    DeleteCompanyProductDialogComponent,
    UnitTypesComponent,
    UnitTypeComponent,
    DeleteUnitTypeDialogComponent,
    CommercesComponent,
    DeleteCommerceDialogComponent,
    AdminCommerceComponent,
    OptionsComponent,
    OptionProductStorageComponent,
    DeleteElementStorageDialogComponent,
    DeleteStorageDialogComponent,
    OptionExtraDataProductTypeComponent,
    DeleteElementExtraDataDialogComponent,
    DeleteExtraDataDialogComponent,
    StockComponent,
    StockTypesComponent,
    DeleteStockTypeDialogComponent,
    StockTypeComponent,
    ProductTypesComponent,
    DeleteProductTypeDialogComponent,
    ProductTypeComponent,
    SaleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule,
    TagInputModule
  ],
  entryComponents: [
    DeleteSupplierDialogComponent,
    DeleteBranchDialogComponent,
    DeleteUserDialogComponent,
    DeleteProductDialogComponent,
    DeleteCompanyProductDialogComponent,
    DeleteUnitTypeDialogComponent,
    DeleteCommerceDialogComponent,
    DeleteElementStorageDialogComponent,
    DeleteStorageDialogComponent,
    DeleteStockTypeDialogComponent,
    DeleteProductTypeDialogComponent,
    DeleteElementExtraDataDialogComponent,
    DeleteExtraDataDialogComponent
  ],
  providers: [MessageService, UserService, SupplierService,
              CommerceService, BranchCommerceService, UserRoleService,
              ProductService, CompanyProductService, UnitTypeService,
              TemplateService, TemplateTypeService, TemplateValueTypeService,
              ProductStockService, ProductTypeService, 
              {provide: HTTP_INTERCEPTORS, useClass: CustomHTTPInterceptor, multi: true},
              { provide: LOCALE_ID, useValue: 'es' },
              ErrorMessagesUtil, RoleUtil, AuthGuard],
  bootstrap: [AppComponent]
})

export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
