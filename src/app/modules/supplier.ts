export class Supplier {
  id: number;
  name: string;
  address: string;
  city: string;
  cityCode: string;
  province: string;
  country: string;
  phone: string;
  cellphone: string;
  email: string;

  constructor(name?: string,
    address?: string,
    city?: string,
    cityCode?: string,
    province?: string,
    country?: string,
    phone?: string,
    cellphone?: string,
    email?: string,
    id?: number) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.city = city;
    this.cityCode = cityCode;
    this.province = province;
    this.country = country;
    this.phone = phone;
    this.cellphone = cellphone;
    this.email = email;
  }

}
