import { TemplateElement } from './template-element';

export class TemplateValue {

    id: number;
    valuez: string[];
    templateElement: TemplateElement;

    constructor(valuez: string[], templateElement: TemplateElement) {
        this.valuez = valuez;
        this.templateElement = templateElement;
    }
}
