
import {TemplateValueType} from './template-value-type';

export class TemplateElement {
  id: number;
  name: string;
  templateValueType: TemplateValueType;
  
  constructor(id: number, name: string, templateValueType: TemplateValueType) {
	  this.id = id;
	  this.name = name;
	  this.templateValueType = templateValueType;
  }
}