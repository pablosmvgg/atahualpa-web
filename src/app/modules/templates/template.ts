
import {TemplateType} from './template-type';
import { TemplateElement } from './template-element';
import { Commerce } from './../commerce';


export class Template {
  id: number;
  name: string;
  templateType: TemplateType;
  templateElements: TemplateElement[];
  commerce: Commerce;

  constructor(templateElements: TemplateElement[]) {
    this.templateElements = templateElements;
  }
}