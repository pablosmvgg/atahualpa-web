import { MessageType } from './message-type';
export class Message {
  message: string;
  type: MessageType;

  constructor(messageString: string, type: MessageType) {
    this.message = messageString;
    this.type = type;
  }

}
