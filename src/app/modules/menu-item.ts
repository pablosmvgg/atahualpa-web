import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { FunctionDeclaration } from 'typescript';

export class MenuItem {
  name: string;
  url: string;
  isAction: boolean;
  action: Function;
  router: Router;
  userService: UserService;

  constructor(name: string, url: string, isAction: boolean) {
      this.name = name;
      this.url = url;
      this.isAction = isAction;
  }

}
