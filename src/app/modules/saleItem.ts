import {ParentSavedResponse} from './parent-saved-response';
import { ProductStock } from './product-stock';

export class SaleItem extends ParentSavedResponse {
  id: number;
  productStock: ProductStock;
  productPrice: number;
  productQuantity: number;
}
