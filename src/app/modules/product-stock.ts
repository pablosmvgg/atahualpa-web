import {Product} from './product';
import { BranchCommerce } from './branch-commerce';
import { ProductStockType } from './product-stock-type';
import { TemplateValue } from './templates/template-value';

export class ProductStock {
  id: number;
  quantity: number;
  minQuantity: number;
  branchCommerce: BranchCommerce;
  product: Product;
  productStockType: ProductStockType;
  storageValues: TemplateValue[];

}
