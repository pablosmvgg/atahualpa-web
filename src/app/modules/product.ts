import {UnitType} from './unit-type';
import {CompanyProduct} from './company-product';
import {ProductType} from './product-type';
import {TemplateValue} from './templates/template-value';

export class Product {
  id: number;
  name: string;
  description: string;
  productType: ProductType;
  price: number;
  percentageProfit: number;
  finalPrice: number;
  unitType: UnitType;
  unitAmount: number;
  barCode: number;
  companyProduct: CompanyProduct;
  minQuantity: number;
  extraFields: TemplateValue[];


  constructor(name?: string) {
    this.name = name;
  }
}
