import {BranchCommerce} from './branch-commerce';
import {UserRole} from './user-role';
import { Commerce } from './commerce';

export class User {
  id: number;
  username: string;
  password: string;
  branchCommerces: BranchCommerce[];
  userRoles: UserRole[];
  name: string;
  email: string;
  sessionId: string;
  commerce: Commerce;
  currentBranchCommerce: BranchCommerce;
}
