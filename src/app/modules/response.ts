import {ParentSavedResponse} from './parent-saved-response';

export class Response {
  status: number;
  message: string;
  savedElement: ParentSavedResponse

}
