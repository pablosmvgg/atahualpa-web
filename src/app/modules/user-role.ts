export class UserRole {
 
  id: number;
  name: string;
  description: string;

  public ROLE_ADMIN:number = 1;
  public ROLE_COMMERCE_OWNER:number = 2;
  public ROLE_COMMERCE_EMPLOYEE:number = 3;
  

}
