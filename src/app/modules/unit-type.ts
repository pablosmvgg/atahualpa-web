export class UnitType {

  id: number;
  name: string;
  shortName: string;

}
