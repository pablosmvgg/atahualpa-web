import {ParentSavedResponse} from './parent-saved-response';

export class Commerce extends ParentSavedResponse {
  id: number;
  name: string;
}