
import {MenuItem} from './menu-item';

export class Menu {
	name:string;
	menuItems:MenuItem[];

	constructor(name: string, menuItems: MenuItem[]) {
		this.name = name;
		this.menuItems = menuItems;
	}

}