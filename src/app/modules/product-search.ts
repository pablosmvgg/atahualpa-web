import { ProductStock } from './product-stock';

export class ProductSearch {
  productName: string;
  cbProductStock: ProductStock;
  productStocks: ProductStock[];
}
