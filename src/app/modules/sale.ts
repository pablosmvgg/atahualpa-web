import {ParentSavedResponse} from './parent-saved-response';
import { SaleItem } from './saleItem';
import { User } from './user';

export class Sale extends ParentSavedResponse {
  id: number;
  saleDate: string;
  items: SaleItem[];
  seller: User;

}
